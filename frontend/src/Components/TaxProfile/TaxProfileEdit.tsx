import React, {FormEvent, useState} from "react";
import Api from "../../Api/Api";
import Profile from "../../Models/Profile";
import {Redirect, RouteComponentProps} from "react-router-dom";
import PaginatedModel from "../../Models/PaginatedModel";
import { connect } from "react-redux";
import {actionGenerator} from "../../State/ActionGenerator";
import Actions from "../../State/Actions";

interface TaxProfileEditState {
    isSubmitted: boolean;
    isLoading: boolean;
    shouldGoBack: boolean;
}

interface NavProps {
    id?: string;
}

interface TaxProfileEditProps extends RouteComponentProps<NavProps> {
    actionGenerator(action: string, key: string, data: PaginatedModel<Profile>): void;
    profiles: Profile[]
}

class TaxProfileEdit extends React.Component<TaxProfileEditProps, TaxProfileEditState> {

    private model: Profile;

    constructor(props: TaxProfileEditProps) {
        super(props);

        this.state = {
            isSubmitted: false,
            shouldGoBack: false,
            isLoading: true
        };

        this.model = new Profile();
    }

    async componentDidMount(): Promise<void> {
        const id = this.props.match.params.id as string;

        const filteredProfiles = this.props.profiles.filter(p => p.id === parseInt(id));
        if (filteredProfiles.length > 0) {
            this.model = filteredProfiles[0];
        } else {
            this.model = await Api.get<Profile>(`tax-profile/one/${id}`);
            if (this.model !== null) {
                const model = new PaginatedModel<Profile>();
                model.data.push(this.model);
                this.props.actionGenerator(Actions.TAX_PROFILE_LOADED, "profiles", model);
            }
        }

        this.setState({isLoading: false});
    }

    async handleSubmit(e: FormEvent): Promise<void> {
        e.preventDefault();

        const result = await Api.put<Profile>(this.model, "tax-profile/update");

        if (result) {
            const resultModel = new PaginatedModel<Profile>();
            resultModel.data.push(this.model);
            this.props.actionGenerator(Actions.TAX_PROFILE_EDITED, "profiles", resultModel);

            this.setState({
                isSubmitted: true
            });
        }
    }

    handleCancelClick(): void {
        this.setState({
            shouldGoBack: true
        });
    }

    handleInputChange(e: any): void {
        (this.model as any)[e.target.name] = e.target.value;
    }
    
    render(): JSX.Element { 
        if (this.state.shouldGoBack || this.state.isSubmitted) {
            return <Redirect to={"/tax-profiles"} />;
        }    

        if (this.state.isLoading) {
            return <h1>Loading...</h1>;
        }

        return (
            <div className={"row"}>
                <div className={"col-lg-12"}>
                    <div className={"ibox"}>
                        <div className={"ibox-title"}>
                            <h5>Create Tax Profile</h5>
                        </div>
                        <div className={"ibox-content"}>
                            <form onSubmit={this.handleSubmit.bind(this)}>
                                <div className={"form-group row"}>
                                    <label className={"col-sm-2 col-form-label"}>Name</label>
                                    <div className={"col-sm-10"}>
                                        <input type={"text"} name={"name"} className={"form-control"} onChange={this.handleInputChange.bind(this)} defaultValue={this.model.name} />
                                    </div>
                                </div>
                                <div className={"hr-line-dashed"} />
                                <div className={"form-group row"}>
                                    <label className={"col-sm-2 col-form-label"}>Description</label>
                                    <div className={"col-sm-10"}>
                                        <textarea name={"description"} rows={5} className={"form-control"} onChange={this.handleInputChange.bind(this)} defaultValue={this.model.description} />
                                    </div>
                                </div>
                                <div style={{display: "flex", justifyContent: "space-between"}}>
                                    <button className={"btn btn-secondary"} onClick={this.handleCancelClick.bind(this)}>Cancel</button>
                                    <button className={"btn btn-primary"}>Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        ); 
    }
}

function mapStateToProps(state: any) {
    console.log(state);
    return {
        profiles: state.TaxProfileReducer
    };
}

export default connect(mapStateToProps, {actionGenerator})(TaxProfileEdit);