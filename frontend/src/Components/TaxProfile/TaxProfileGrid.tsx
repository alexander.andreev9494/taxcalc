import React from "react";
import { connect } from "react-redux";
import {Link} from "react-router-dom";
import Api from "../../Api/Api";
import PaginatedModel from "../../Models/PaginatedModel";
import Profile from "../../Models/Profile";
import {actionGenerator} from "../../State/ActionGenerator";
import Actions from "../../State/Actions";

interface ITaxProfileProps {
    actionGenerator(action: string, key: string, data: PaginatedModel<Profile>): void;
    profiles: Profile[]
}

class TaxProfileGrid extends React.Component<ITaxProfileProps, any> {

    constructor(props: ITaxProfileProps) {
        super(props);
    }

    async componentDidMount() {
        const result = await Api.get<PaginatedModel<Profile>>("tax-profile");
        this.props.actionGenerator(Actions.TAX_PROFILES_LOADED, "profiles", result);
    }

    async handleRemove(id: number) {
        const result = await Api.delete(`tax-profile/delete/${id}`);
        if (result) {
            const model = new PaginatedModel<Profile>();
            model.data = this.props.profiles.filter(p => p.id === id);

            this.props.actionGenerator(Actions.TAX_PROFILE_REMOVED, "profiles", model);
        }
    }

    render() {
        return (
            <div className={"row"}>
                <div className={"col-lg-12"}>
                    <div className={"ibox"}>
                        <div className="ibox-title ibox-title-formatting">
                            <div>
                                <h5>Tax Profiles</h5>
                            </div>
                            <div>
                                <Link to={"tax-profiles/create"} className={"btn btn-primary"}>Create</Link>
                            </div>
                        </div>
                        <div className="ibox-content">
                            <table className={"table"}>
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Name</th>
                                        <th>Description</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                {this.props.profiles?.map(p => (
                                    <tr key={p.id}>
                                        <td>{p.id}</td>
                                        <td>{p.name}</td>
                                        <td>{p.description}</td>
                                        <td className={"table-actions-column-formatting"}>
                                            <Link to={"tax-profiles/edit/" + p.id} className={"btn btn-warning"}>Edit</Link>
                                            <button className={"btn btn-danger"} onClick={() => this.handleRemove(p.id as number)}>Remove</button>
                                        </td>
                                    </tr>
                                ))}
                                </tbody>
                            </table>
                            {/* <ul className="pagination">
                                <li className="page-item"><a className="page-link" href="#">Previous</a></li>
                                <li className="page-item"><a className="page-link" href="#" onClick={() => handlePagination(1)}>1</a></li>
                                <li className="page-item"><a className="page-link" href="#" onClick={() => handlePagination(2)}>2</a></li>
                                <li className="page-item"><a className="page-link" href="#">3</a></li>
                                <li className="page-item"><a className="page-link" href="#">Next</a></li>
                            </ul> */}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state: any): {profiles: Profile[]} {
    return {
        profiles: state.TaxProfileReducer
    };
}


export default connect(mapStateToProps, {actionGenerator})(TaxProfileGrid);