import React, {FormEvent, useState} from "react";
import Api from "../../Api/Api";
import Profile from "../../Models/Profile";
import {Redirect} from "react-router-dom";
import PaginatedModel from "../../Models/PaginatedModel";
import { connect } from "react-redux";
import {actionGenerator} from "../../State/ActionGenerator";
import Actions from "../../State/Actions";

interface TaxProfileCreateState {
    isSubmitted: boolean;
    shouldGoBack: boolean;
}

interface TaxProfileCreateProps {
    actionGenerator(action: string, key: string, data: PaginatedModel<Profile>): void;
}

class TaxProfileCreate extends React.Component<TaxProfileCreateProps, TaxProfileCreateState> {

    private model: Profile;

    constructor(props: TaxProfileCreateProps) {
        super(props);

        this.state = {
            isSubmitted: false,
            shouldGoBack: false
        };

        this.model = new Profile();
    }

    async handleSubmit(e: FormEvent) {
        e.preventDefault();

        const response = await Api.post<Profile>(this.model, "tax-profile/create");

        if (response.id !== -1) {
            const resultModel = new PaginatedModel<Profile>();
            resultModel.data.push(this.model);
            this.props.actionGenerator(Actions.TAX_PROFILE_CREATED, "profiles", resultModel);

            this.setState({
                isSubmitted: true
            });
        }
    }

    handleCancelClick() {
        this.setState({
            shouldGoBack: true
        });
    }

    handleInputChange(e: any) {
        (this.model as any)[e.target.name] = e.target.value;
    }
    
    render() { 
        if (this.state.shouldGoBack || this.state.isSubmitted) {
            return <Redirect to={"/tax-profiles"} />;
        }    

        return (
            <div className={"row"}>
                <div className={"col-lg-12"}>
                    <div className={"ibox"}>
                        <div className={"ibox-title"}>
                            <h5>Create Tax Profile</h5>
                        </div>
                        <div className={"ibox-content"}>
                            <form onSubmit={this.handleSubmit.bind(this)}>
                                <div className={"form-group row"}>
                                    <label className={"col-sm-2 col-form-label"}>Name</label>
                                    <div className={"col-sm-10"}>
                                        <input type={"text"} name={"name"} className={"form-control"} onChange={this.handleInputChange.bind(this)} />
                                    </div>
                                </div>
                                <div className={"hr-line-dashed"} />
                                <div className={"form-group row"}>
                                    <label className={"col-sm-2 col-form-label"}>Description</label>
                                    <div className={"col-sm-10"}>
                                        <textarea name={"description"} rows={5} className={"form-control"} onChange={this.handleInputChange.bind(this)} />
                                    </div>
                                </div>
                                <div style={{display: "flex", justifyContent: "space-between"}}>
                                    <button className={"btn btn-secondary"} onClick={this.handleCancelClick.bind(this)}>Cancel</button>
                                    <button className={"btn btn-primary"}>Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        ); 
    }
}

function mapStateToProps(state: any) {
    return {

    }
}

export default connect(mapStateToProps, {actionGenerator})(TaxProfileCreate);