import React, { useState } from "react";
import { Link } from "react-router-dom";

interface ILayoutProps {
    children: any
}

export const Layout = (props: ILayoutProps) => {

    const [menuHiddenState, setMenuState] = useState<boolean>();

    const toggleMenu = () => {
        const navbarClass = "mini-navbar";
        const body = document.querySelector("body") as HTMLBodyElement;
        if (menuHiddenState) {
            setMenuState(false);
            body.classList.remove(navbarClass);
        } else {
            setMenuState(true); //70px
            body.classList.add(navbarClass);
        }
    }

    return (<div id="wrapper">
        <nav className="navbar-default navbar-static-side" role="navigation" style={{width: menuHiddenState ? "70px" : "220px", transition: menuHiddenState ? "margin-right 1s" : "margin-left 1s"}}>
            <div className="sidebar-collapse">
                <ul className="nav metismenu" id="side-menu">
                    <li className="nav-header">
                        <div className="logo-element">
                            IN+
                        </div>
                    </li>
                    <li>
                        <Link to={"/tax-profiles"}><i className="fa fa-diamond"></i> <span
                            className="nav-label">Tax Profiles</span></Link>
                    </li>
                    <li>
                        <Link to={"/taxes"}><i className="fa fa-diamond"></i> <span
                            className="nav-label">Taxes</span></Link>
                    </li>
                </ul>

            </div>
        </nav>

        <div id="page-wrapper" className="gray-bg">
            <div className="row border-bottom">
                <nav className="navbar navbar-static-top white-bg" role="navigation" style={{marginBottom: 0}}>
                    <div className="navbar-header">
                        <a className="navbar-minimalize minimalize-styl-2 btn btn-primary" onClick={toggleMenu}>
                            <i className="fa fa-bars"></i>
                        </a>
                    </div>
                    <ul className="nav navbar-top-links navbar-right">
                        <li>
                            <a href="login.html">
                                <i className="fa fa-sign-out"></i> Log out
                            </a>
                        </li>
                    </ul>

                </nav>
            </div>
            <div className="wrapper wrapper-content">
                {props.children}
            </div>
            <div className="footer">
                <div className="float-right">
                    10GB of <strong>250GB</strong> Free.
                </div>
                <div>
                    <strong>Copyright</strong> Example Company &copy; 2014-2018
                </div>
            </div>

        </div>
    </div>)
}