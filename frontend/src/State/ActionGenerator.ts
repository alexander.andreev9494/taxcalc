import dispatch from "redux-thunk";
import Api from "../Api/Api";
import PaginatedModel from "../Models/PaginatedModel";
import Profile from "../Models/Profile";
import Actions from "./Actions";

export const actionGenerator = (action: string, key: string, data: PaginatedModel<any>) => async (dispatch: any) => {
    dispatch({
        type: action, 
        [key]: data
    });
}
