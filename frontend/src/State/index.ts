import {applyMiddleware, combineReducers, createStore} from "redux";
import TaxProfileReducer from "./TaxProfileReducer";
import TaxReducer from "./TaxReducer";
import thunk from "redux-thunk";

export const store = createStore(combineReducers({TaxProfileReducer, TaxReducer}), applyMiddleware(thunk));