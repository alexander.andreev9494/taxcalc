import Actions from "./Actions";
import Profile from "../Models/Profile";
import PaginatedModel from "../Models/PaginatedModel";

interface ProfilePayload {
    profiles: PaginatedModel<Profile>;
    type: string;
}

export default (state: Array<Profile> = [], payload: ProfilePayload) => {

    switch(payload.type) {
        case Actions.TAX_PROFILES_LOADED:
            return [...payload.profiles.data];
        case Actions.TAX_PROFILE_REMOVED:
            return [...state.filter(t => t.id !== payload.profiles.data[0].id)];
        case Actions.TAX_PROFILE_EDITED:
            return [...state.filter(t => t.id !== payload.profiles.data[0].id), ...payload.profiles.data];
        case Actions.TAX_PROFILE_CREATED:
            return [...state, ...payload.profiles.data];
    }

    return state;
}