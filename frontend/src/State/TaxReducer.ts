import PaginatedModel from "../Models/PaginatedModel";
import Tax from "../Models/Tax";
import Actions from "./Actions";

interface TaxPayload {
    taxes: PaginatedModel<Tax>;
    type: string;
}

export default (state: Array<Tax> = [], payload: TaxPayload) => {
    switch (payload.type) {
        case Actions.TAXES_LOADED:
            return [...payload.taxes.data];
        case Actions.TAX_CREATED:
            return [...state, ...payload.taxes.data];
        case Actions.TAX_EDITED:
            return [...state.filter(t => t.id === payload.taxes.data[0].id), ...payload.taxes.data];
        case Actions.TAX_REMOVED:
            return [...state.filter(t => t.id === payload.taxes.data[0].id)]; 
    }

    return state;
}