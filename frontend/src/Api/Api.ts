import axios from "axios";
import CreateResponse from "./CreateResponse";

export default class Api {
    private static baseUrl: string = "http://localhost:8000/api/backoffice/"

    static async get<TModel>(url: string): Promise<TModel> {
        var response = await axios.get(this.baseUrl + url);
        return response.data
    }

    static async post<TModel>(model: TModel, url: string): Promise<CreateResponse> {
        var response = await axios.post(this.baseUrl + url, model);
        return response.data;
    }

    static async put<TModel>(model: TModel, url: string): Promise<boolean> {
        const response = await axios.put(this.baseUrl + url, model);
        return response.status === 200;
    }

    static async delete(url: string): Promise<boolean> {
        const response = await axios.delete(this.baseUrl + url);
        return response.status === 200;
    }
}