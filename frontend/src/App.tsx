import React from 'react';
import './App.css';
import {BrowserRouter, Switch, Route, Redirect} from "react-router-dom";
import TaxProfileGrid from "./Components/TaxProfile/TaxProfileGrid";
import {Layout} from "./Components/Layout";
import TaxProfileCreate from "./Components/TaxProfile/TaxProfileCreate";
import TaxProfileEdit from "./Components/TaxProfile/TaxProfileEdit"; 

/* import {TaxGrid} from "./Components/Tax/TaxGrid";
import {TaxEdit} from "./Components/Tax/TaxEdit"; */
import { Provider } from 'react-redux';

import {store} from "./State/index";

function App() {
  return (
        <Provider store={store}>
            <BrowserRouter>
                <Layout>
                    <Switch>
                        <Route exact path={"/"} component={() => <Redirect to={"/tax-profiles"}/>} />
                        <Route exact path={"/tax-profiles"} component={TaxProfileGrid}/>
                        <Route exact path={"/tax-profiles/create"} component={TaxProfileCreate}/>
                        <Route exact path={"/tax-profiles/edit/:id"} component={TaxProfileEdit}/>
                        {/* <Route exact path={"/taxes"} component={TaxGrid} />
                        <Route exact path={"/taxes/edit/:id"} component={TaxEdit} /> */}
                    </Switch>
                </Layout>
            </BrowserRouter>
        </Provider>
          
  );
}

export default App;
