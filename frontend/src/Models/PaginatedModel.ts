import BaseModel from "./BaseModel";

export default class PaginatedModel<TModel extends BaseModel> {
    public total: number = 0;
    public page: number = 0;
    public pageSize: number = 0;
    public data: TModel[] = new Array<TModel>();

    constructor() {
        this.data = new Array<TModel>();
    }
}