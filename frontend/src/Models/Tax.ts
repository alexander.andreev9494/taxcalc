import BaseModel from "./BaseModel";
import Profile from "./Profile";

export default class Tax extends BaseModel {
    

    constructor(id: number,
                public title: string = "",
                public description: string = "",
                public amount: number = 0,
                public isPercentage: boolean = false,
                public appliesBefore: number|null = null,
                public profile: Profile|null = null,
                public profileId: number) {
        super();
        this.id = id;
        this.profileId = this.profile?.id ?? this.profileId;
    }
}