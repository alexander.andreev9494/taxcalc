import BaseModel from "./BaseModel";

export default class Profile extends BaseModel {
    public name: string = "";
    public description: string = "";
}