<?php


namespace App\Infrastructure\Serializers;


use App\Domain\Contracts\SerializerInterface;
use JMS\Serializer\SerializerInterface as JmsSerializerInterface;

class JsonSerializer implements SerializerInterface
{
    public function __construct(private JmsSerializerInterface $serializer)
    {
    }

    public function serialize(object|array $data): string
    {
        return $this->serializer->serialize($data, "json");
    }

    public function deserialize(string $className, string $data): array|object
    {
        return $this->serializer->deserialize($data, $className, "json");
    }

}