<?php

namespace App\Infrastructure\Http\EventListeners;

use App\Domain\Metadata\Services\ClassLoaderServiceInterface;
use App\Domain\Metadata\Services\MetadataServiceInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;

class RequestSubscriber implements EventSubscriberInterface
{
    public function __construct(private readonly ClassLoaderServiceInterface $classLoaderService,
                                private readonly MetadataServiceInterface $metadataService)
    {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            RequestEvent::class => 'onKernelRequest',
        ];
    }

    public function onKernelRequest(RequestEvent $event): void
    {
        $classes = $this->classLoaderService->loadAllClasses();
        $this->metadataService->registerMetadataForClasses($classes);
    }
}