<?php


namespace App\Infrastructure\Http\Controllers;


use App\Domain\Contracts\SerializerInterface;
use App\Domain\Cqrs\Contracts\CommandBusInterface;
use App\Domain\Cqrs\Contracts\QueryBusInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;

abstract class ApiControllerBase extends AbstractController
{
    public function __construct(protected SerializerInterface $serializer,
                                protected QueryBusInterface $queryBus,
                                protected CommandBusInterface $commandBus,
                                private RequestStack $requestStack)
    {
    }

    protected function ok($data = null): JsonResponse
    {
        $json = false;
        if (is_array($data) || is_object($data))
        {
            $data = $this->serializer->serialize($data);
            $json = true;
        }

        return new JsonResponse($data, json: $json);
    }

    protected function created($data = null): JsonResponse
    {
        $json = false;
        if (is_array($data) || is_object($data))
        {
            $data = $this->serializer->serialize($data);
            $json = true;
        }

        return new JsonResponse($data, Response::HTTP_CREATED, json: $json);
    }

    protected function badRequest($data): JsonResponse
    {
        $json = false;
        if (is_array($data) || is_object($data))
        {
            $data = $this->serializer->serialize($data);
            $json = true;
        }

        return new JsonResponse($data, Response::HTTP_BAD_REQUEST, json: $json);
    }

    protected function notFound($data = null): JsonResponse
    {
        $json = false;
        if (is_array($data) || is_object($data))
        {
            $data = $this->serializer->serialize($data);
            $json = true;
        }

        return new JsonResponse($data, Response::HTTP_NOT_FOUND, json: $json);
    }

    protected function getPage(): int
    {
        return $this->requestStack->getCurrentRequest()->get("page") ?? 1;
    }

    protected function getPageSize(): int 
    {
        return $this->requestStack->getCurrentRequest()->get("pageSize") ?? 10;
    }

    protected function mapRequestBody(string $modelClass): object 
    {
        $request = $this->requestStack->getCurrentRequest();
        $json = $request->getContent();

        return $this->serializer->deserialize($modelClass, $json);
    }
}