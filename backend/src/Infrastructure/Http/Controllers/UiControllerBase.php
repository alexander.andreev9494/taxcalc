<?php


namespace App\Infrastructure\Http\Controllers;


use App\Domain\Cqrs\CommandResult;
use App\Domain\Cqrs\Contracts\CommandBusInterface;
use App\Domain\Cqrs\Contracts\QueryBusInterface;
use App\Domain\Mapping\MapperInterface;
use App\Domain\Validation\ValidationResult;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Form\FormInterface;

class UiControllerBase extends AbstractController
{
    public function __construct(protected CommandBusInterface $commandBus,
                                protected QueryBusInterface $queryBus,
                                protected MapperInterface $mapper,
                                private RequestStack $requestStack)
    {
    }

    protected function getPage(): int
    {
        return $this->requestStack->getCurrentRequest()->get("page") ?? 1;
    }

    protected function getPageSize(): int
    {
        return $this->requestStack->getCurrentRequest()->get("pageSize") ?? 10;
    }

    protected function setValidationResults(CommandResult $commandResult, FormInterface $form): bool
    {
        $hasErrors = false;
        if (!$commandResult->isSuccess() && $commandResult->hasErrors())
        {
            /** @var ValidationResult[] $errors */
            $errors = $commandResult->getMessages();

            foreach ($errors as $error)
            {
                $form->addError(new FormError($error->getMessage()));
            }

            $hasErrors = true;
        }

        return $hasErrors;
    } 
}