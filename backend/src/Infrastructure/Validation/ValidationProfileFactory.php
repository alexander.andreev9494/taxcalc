<?php


namespace App\Infrastructure\Validation;


use App\Domain\Metadata\Services\MetadataServiceInterface;
use App\Domain\Validation\Contracts\ValidationProfileFactoryInterface;
use App\Domain\Validation\Contracts\ValidationProfileInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

final class ValidationProfileFactory implements ValidationProfileFactoryInterface
{
    public function __construct(private readonly ContainerInterface $container,
                                private readonly MetadataServiceInterface $metadataService)
    {
        
    }

    public function create(string $profileClass): ValidationProfileInterface
    {
        $parameters = $this->metadataService->getMetadataByClass($profileClass)?->constructorParameters;
        
        $args = array_map(fn (string $p) => $this->container->get($p), $parameters);
        return new $profileClass(...$args);
    }
}