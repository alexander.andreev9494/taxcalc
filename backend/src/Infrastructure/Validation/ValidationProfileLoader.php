<?php


namespace App\Infrastructure\Validation;


use App\Application\Abstractions\BaseValidationProfile;
use App\Domain\Metadata\ClassPath;
use App\Domain\Metadata\Services\ClassLoaderServiceInterface;
use App\Domain\Validation\Contracts\ValidationProfileFactoryInterface;
use App\Domain\Validation\Contracts\ValidationProfileInterface;
use App\Domain\Validation\Contracts\ValidationProfileLoaderInterface;
use ReflectionClass;

final class ValidationProfileLoader implements ValidationProfileLoaderInterface
{
    private const VALIDATION_PROFILES_KEY = "validation_profiles";

    public function __construct(private readonly string $env,
                                private readonly ValidationProfileFactoryInterface $validationProfileFactory,
                                private readonly ClassLoaderServiceInterface $classLoaderService)
    {
    }

    public function load(): void
    {
        /** @var ValidationProfileInterface $profile */
        foreach ($this->getProfiles() as $profile)
        {
            $profile->register();
        }
    }

    private function getProfiles(): array
    {
        if ($this->env === "dev")
        {
            $profileClasses = $this->getProfileClassesFromFs();
            return array_map(fn (string $profileClass) => $this->validationProfileFactory->create($profileClass), $profileClasses);
        } 
        else
        {
            if (apcu_exists(self::VALIDATION_PROFILES_KEY))
            {
                $profileClasses = (array) apcu_fetch(self::VALIDATION_PROFILES_KEY);
                return array_map(fn (string $profileClass) => $this->validationProfileFactory->create($profileClass), $profileClasses);
            }
            else 
            {
                $profileClasses = $this->getProfileClassesFromFs();
                apcu_store(self::VALIDATION_PROFILES_KEY, $profileClasses);
                return array_map(fn (string $profileClass) => $this->validationProfileFactory->create($profileClass), $profileClasses);
            }
        }
    }

    private function getProfileClassesFromFs(): array
    {
        $classes = $this->classLoaderService->loadClassesByPath(ClassPath::Application);

        $result = [];

        foreach ($classes as $class)
        {
            $reflection = new ReflectionClass($class);
            $parentReflection = $reflection->getParentClass();
            if (!$parentReflection)
            {
                continue;
            }

            $parentClass = $parentReflection->getName();
            if ($parentClass === BaseValidationProfile::class && $class !== self::class)
            {
                $result[] = $class;
            }
        }

        return $result;
    }
}