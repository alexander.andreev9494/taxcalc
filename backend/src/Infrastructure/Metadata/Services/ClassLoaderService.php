<?php

namespace App\Infrastructure\Metadata\Services;

use App\Domain\Metadata\ClassPath;
use App\Domain\Metadata\Services\ClassLoaderServiceInterface;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use RegexIterator;

class ClassLoaderService implements ClassLoaderServiceInterface
{
    private const CACHE_CLASSES_KEY = "app_classes_cache";

    public function __construct(private readonly string $projectDir)
    {
    }

    public function loadClassesByPath(ClassPath $classPath): array
    {
        if (!apcu_exists(self::CACHE_CLASSES_KEY))
        {
            apcu_store(self::CACHE_CLASSES_KEY, []);
        }

        $result = (array) apcu_fetch(self::CACHE_CLASSES_KEY);

        $path = $this->projectDir . $classPath->value;

        if (!isset($result[$path]))
        {
            $dirIterator = new RecursiveDirectoryIterator($path);
            $iterator = new RecursiveIteratorIterator($dirIterator);
            $files = new RegexIterator($iterator, '/\.php$/');

            foreach ($files as $file)
            {
                $srcPos = strpos($file, "src");
                $class = substr($file, $srcPos);
                $class = str_replace("src", "App", $class);
                $class = str_replace("/", "\\", $class);
                $class = str_replace(".php", "", $class);

                $result[$path][] = $class;
            }

            apcu_store(self::CACHE_CLASSES_KEY, $result);
        }

        return $result[$path];
    }

    public function loadAllClasses(): array
    {
        $domainClasses = $this->loadClassesByPath(ClassPath::Domain);
        $applicationClasses = $this->loadClassesByPath(ClassPath::Application);

        return [...$applicationClasses, ...$domainClasses];
    }
}