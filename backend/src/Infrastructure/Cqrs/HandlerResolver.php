<?php


namespace App\Infrastructure\Cqrs;


use App\Domain\Cqrs\Attributes\Handler;
use App\Domain\Metadata\Services\MetadataServiceInterface;
use App\Domain\Cqrs\Contracts\CommandInterface;
use App\Domain\Cqrs\Contracts\HandlerResolverInterface;
use App\Domain\Cqrs\Contracts\HandlerFactoryInterface;
use App\Domain\Cqrs\Contracts\QueryInterface;
use App\Infrastructure\Cqrs\Exceptions\CanNotResolveHandlerException;
use App\Infrastructure\Cqrs\Exceptions\HandlerAttributeNotFoundException;
use Closure;

class HandlerResolver implements HandlerResolverInterface
{
    public function __construct(private readonly HandlerFactoryInterface $handlerFactory,
                                private readonly MetadataServiceInterface $metadataService)
    {
    }

    public function resolve(QueryInterface|CommandInterface $object): Closure
    {
        $attribute = $this->metadataService->getMetadataByClass($object::class)?->getAttribute(Handler::class);
        if ($attribute === null)
        {
            throw new HandlerAttributeNotFoundException("Can not find handler for given command/query: ".$object::class);
        }

        $handler = $this->handlerFactory->create($attribute->handlerClass);

        if (is_callable($handler))
        {
            return Closure::fromCallable($handler);
        }

        throw new CanNotResolveHandlerException("Something went wrong during handler resolving for: ".$object::class);
    }


}