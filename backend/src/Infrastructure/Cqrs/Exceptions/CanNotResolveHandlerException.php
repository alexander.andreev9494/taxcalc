<?php 


namespace App\Infrastructure\Cqrs\Exceptions;

use Exception;

class CanNotResolveHandlerException extends Exception
{
    
}