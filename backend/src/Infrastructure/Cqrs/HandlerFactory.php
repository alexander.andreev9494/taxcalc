<?php


namespace App\Infrastructure\Cqrs;


use Exception;
use App\Domain\Cqrs\Contracts\HandlerInterface;
use App\Domain\Metadata\Services\MetadataServiceInterface;
use App\Domain\Cqrs\Contracts\HandlerFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class HandlerFactory implements HandlerFactoryInterface
{
    public function __construct(private readonly ContainerInterface $container,
                                private readonly MetadataServiceInterface $metadataService)
    {

    }

    public function create(string $handlerClass): HandlerInterface
    {
        $metadata = $this->metadataService->getMetadataByClass($handlerClass);

        if ($metadata == null || !$metadata->implementsInterface(HandlerInterface::class))
        {
            throw new Exception("Handler $handlerClass doesn't implement ".HandlerInterface::class." or can't load metadata for class $handlerClass");
        }

        $args = array_map(fn (string $p) => $this->container->get($p), $metadata->constructorParameters);
        return new $handlerClass(...$args);
    }
}