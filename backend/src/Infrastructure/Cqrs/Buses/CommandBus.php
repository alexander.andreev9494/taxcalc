<?php


namespace App\Infrastructure\Cqrs\Buses;


use App\Domain\Cqrs\CommandResult;
use App\Domain\Cqrs\Contracts\CommandBusInterface;
use App\Domain\Cqrs\Contracts\CommandInterface;
use App\Domain\Cqrs\Contracts\HandlerResolverInterface;

class CommandBus implements CommandBusInterface
{
    public function __construct(private HandlerResolverInterface $handlerResolver)
    {
    }

    public function dispatch(CommandInterface $command): CommandResult
    {
        $handler = $this->handlerResolver->resolve($command);
        return $handler($command);
    }

}