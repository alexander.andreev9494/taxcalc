<?php


namespace App\Infrastructure\Cqrs\Buses;


use App\Domain\Cqrs\Contracts\HandlerResolverInterface;
use App\Domain\Cqrs\Contracts\QueryBusInterface;
use App\Domain\Cqrs\Contracts\QueryInterface;

class QueryBus implements QueryBusInterface
{
    public function __construct(private HandlerResolverInterface $handlerResolver)
    {
    }

    public function execute(QueryInterface $query)
    {
        $handler = $this->handlerResolver->resolve($query);
        return $handler($query);
    }

}