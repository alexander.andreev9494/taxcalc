<?php


namespace App\Infrastructure\Database;


use App\Infrastructure\Database\Repositories\GenericRepository;
use App\Domain\Entities\BaseEntity;
use App\Domain\Persistence\DataManagerInterface;
use App\Domain\Persistence\RepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;

class DataManager implements DataManagerInterface
{
    private array $repositories = [];

    public function __construct(private EntityManagerInterface $entityManager)
    {
    }

    public function getRepository(string $className): RepositoryInterface
    {
        if (!isset($this->repositories[$className]))
        {
            $this->repositories[$className] = new GenericRepository($this->entityManager, $className);
        }
        
        return $this->repositories[$className];
    }

    public function persist(BaseEntity $baseEntity): void
    {
        $this->entityManager->persist($baseEntity);
    }

    public function remove(string $className, int $id): void
    {
        $entity = $this->entityManager->getRepository($className)
            ->find($id);

        if ($entity !== null)
        {
            $this->entityManager->remove($entity);
        }
    }

    public function save(): void
    {
        $this->entityManager->flush();
    }

}