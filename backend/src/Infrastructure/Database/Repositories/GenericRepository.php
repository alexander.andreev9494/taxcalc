<?php


namespace App\Infrastructure\Database\Repositories;


use App\Domain\Entities\BaseEntity;
use App\Domain\Persistence\RepositoryInterface;
use Doctrine\ORM\EntityManagerInterface;

class GenericRepository implements RepositoryInterface
{
    public function __construct(protected EntityManagerInterface $entityManager,
                                protected string $entityClass)
    {
    }

    public function count(): int 
    {
        return $this->entityManager->createQueryBuilder()
            ->from($this->entityClass, 'e')
            ->select('count(e.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function findOne(int $id): ?BaseEntity
    {
        return $this->entityManager->find($this->entityClass, $id);
    }

    public function findBy(array $params = [], array $orderBy = []): array
    {
        return $this->entityManager->getRepository($this->entityClass)
            ->findBy($params, $orderBy);
    }

    public function findPaginated(int $page, int $pageSize): array
    {
        return $this->entityManager->createQueryBuilder()
            ->from($this->entityClass, 'e')
            ->select('e')
            ->setMaxResults($pageSize)
            ->setFirstResult($page * $pageSize - $pageSize)
            ->getQuery()
            ->getResult();
    }

    public function forDoropdown(string $keyName, string $valueName): array
    {
        $data = $this->entityManager->createQueryBuilder()
            ->from($this->entityClass, 'e')
            ->select("e.{$keyName}, e.{$valueName}")
            ->getQuery()
            ->getResult();
      
        return array_merge(...array_map(fn (array $data) => [$data["name"] => $data["id"]], $data));
    }
}