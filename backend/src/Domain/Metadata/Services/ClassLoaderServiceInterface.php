<?php

namespace App\Domain\Metadata\Services;

use App\Domain\Metadata\ClassPath;

interface ClassLoaderServiceInterface
{
    public function loadClassesByPath(ClassPath $classPath): array;
    public function loadAllClasses(): array;
}