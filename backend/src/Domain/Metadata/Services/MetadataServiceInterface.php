<?php

namespace App\Domain\Metadata\Services;

use App\Domain\Metadata\ClassDescriptor;

interface MetadataServiceInterface
{
    public function getMetadataByClass(string $className): ?ClassDescriptor;
    public function registerMetadataForClass(string $className): void;
    public function registerMetadataForClasses(array $classes): void;
}