<?php

namespace App\Domain\Metadata;

class ClassDescriptor
{
    /**
     * @param string $className
     * @param object[] $attributes
     * @param PropertyDescriptor[] $properties
     * @param string[] $constructorParameters
     * @param ?string $parentClass = null
     * @param string[] $interfaces
     */
    public function __construct(public readonly string $className,
                                public readonly array $attributes,
                                public readonly array $properties,
                                public readonly array $constructorParameters,
                                public readonly ?string $parentClass,
                                public readonly array $interfaces)
    {

    }

    public function getAttribute(string $attributeClass): ?object
    {
        $result = null;

        $filteredAttributes = array_filter($this->attributes, fn (object $attribute) => $attribute instanceof $attributeClass);
        if (!empty($filteredAttributes))
        {
            $result = $filteredAttributes[0];
        }

        return $result;
    }

    public function implementsInterface(string $interface): bool
    {
        $result = array_filter($this->interfaces, fn(string $i) => $i === $interface);
        return !empty($result);
    }
}