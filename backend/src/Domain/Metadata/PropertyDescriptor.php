<?php

namespace App\Domain\Metadata;

class PropertyDescriptor
{
    public function __construct(public readonly ?string $name,
                                public readonly ?string $type,
                                public readonly ?array $attributes,
                                public ?bool $isPublic)
    {
    }
}