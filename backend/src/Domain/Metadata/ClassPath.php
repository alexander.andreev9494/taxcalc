<?php

namespace App\Domain\Metadata;

enum ClassPath : string
{
    case Domain = "/src/Domain";
    case Application = "/src/Application";
}