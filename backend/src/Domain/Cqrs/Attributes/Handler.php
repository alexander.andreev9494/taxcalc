<?php


namespace App\Domain\Cqrs\Attributes;


use Attribute;

#[Attribute(Attribute::TARGET_CLASS)]
class Handler
{
    public function __construct(public readonly string $handlerClass)
    {
    }
}