<?php


namespace App\Domain\Cqrs;;


class PaginatedQueryResult 
{
    public function __construct(public int $total,
                                public int $page,
                                public int $pageSize, 
                                public array $data)
    {
    }
}