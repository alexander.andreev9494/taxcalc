<?php


namespace App\Domain\Cqrs;


class CommandResult
{
    public const RESULT_OK = 1;
    public const RESULT_FAIL = 2;

    private array $messages;

    private function __construct(private int $resultCode, array $messages = [])
    {
        $this->messages = $messages;
    }

    public function hasErrors(): bool
    {
        return count($this->messages) > 0;
    }

    public function getMessages(): array
    {
        return $this->messages;
    }

    public function isSuccess(): bool
    {
        return $this->resultCode === self::RESULT_OK;
    }

    public static function ok(array $messages = []): self
    {
        return new CommandResult(self::RESULT_OK, $messages);
    }

    public static function fail(array $messages = []): self
    {
        return new CommandResult(self::RESULT_FAIL, $messages);
    }
}