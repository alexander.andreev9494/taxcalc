<?php


namespace App\Domain\Cqrs\Contracts;


use Closure;

interface HandlerResolverInterface
{
    public function resolve(CommandInterface|QueryInterface $object): Closure;
}