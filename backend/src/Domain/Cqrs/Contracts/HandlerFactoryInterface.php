<?php


namespace App\Domain\Cqrs\Contracts;

interface HandlerFactoryInterface
{
    public function create(string $handlerClass): HandlerInterface;
}