<?php


namespace App\Domain\Cqrs\Contracts;


interface QueryBusInterface
{
    public function execute(QueryInterface $query);
}