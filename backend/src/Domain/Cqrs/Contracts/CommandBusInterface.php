<?php


namespace App\Domain\Cqrs\Contracts;


use App\Domain\Cqrs\CommandResult;

interface CommandBusInterface
{
    public function dispatch(CommandInterface $command): CommandResult;
}