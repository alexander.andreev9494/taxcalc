<?php


namespace App\Domain\Validation;


use App\Domain\Entities\BaseEntity;
use App\Domain\Validation\Contracts\CustomValidatorInterface;
use Closure;
use ReflectionClass;
use ReflectionProperty;
use App\Domain\Validation\Contracts\ValidationEngineInterface;
use App\Domain\Validation\Contracts\ValidationContextInterface;
use App\Domain\Validation\Contracts\ValidationResultInterface;

class ValidationEngine implements ValidationEngineInterface
{
    /** @var ValidationContainer[] */
    private array $validationContainers = [];

    public function forEntity(string $entityClass, Closure $builder): void
    {
        $this->validationContainers[$entityClass] ??= new ValidationContainer();

        $this->validationContainers[$entityClass]->validationBuilder ??= new ValidationBuilder();

        $builder($this->validationContainers[$entityClass]->validationBuilder);
    }

    public function validate(BaseEntity $entity): ValidationResults
    {
        if (!isset($this->validationContainers[$entity::class]))
        {
            return new ValidationResults();
        }

        $validationRules = $this->validationContainers[$entity::class]->validationBuilder->build();
        $validationContext = $this->validationContainers[$entity::class]->validationContext ?? null;

        $props = $this->getProperties($entity);

        $results = [];
        foreach ($props as $propName => $value)
        {
            if (!isset($validationRules[$propName]))
            {
                continue;
            }

            $propValidationRules = $validationRules[$propName];
            foreach ($propValidationRules as $rule => $ruleValue)
            {
                if (!$ruleValue)
                {
                    continue;
                }

                if ($rule === "withCustomValidator")
                {
                    $results = [...$results, ...$this->runCustomValidators($ruleValue, $propName, $value, $validationContext)];
                }
                else
                {
                    $results[] = match ($rule)
                    {
                        "required" => $this->checkRequired($entity::class, $propName, $value),
                        "minLength" => $this->checkMinLength($entity::class, $propName, $value, $ruleValue),
                        "maxLength" => $this->checkMaxLength($entity::class, $propName, $value, $ruleValue),
                        "inRange" => $this->checkInRange($entity::class, $propName, $value, $ruleValue[0], $ruleValue[1])
                    };
                }
            }
        }

        $errors = array_filter($results, fn (ValidationResult $vr) => $vr->getStatus() === ValidationResult::STATUS_ERROR);
        $warnings = array_filter($results, fn (ValidationResult $vr) => $vr->getStatus() === ValidationResult::STATUS_WARNING);

        return new ValidationResults($errors, $warnings);
    }

    public function withValidationContext(string $entityClass, ValidationContextInterface $validationContext): void
    {
        $this->validationContainers[$entityClass]->validationContext = $validationContext;
    }

    private function runCustomValidators(array $validators, string $propName, $value, ?ValidationContextInterface $validationContext): array
    {
        $validationResults = [];
        foreach($validators as $validator)
        {
            $validationResults[] = $validator($propName, $value, $validationContext);
        }

        return $validationResults;
    }

    private function getProperties(object $entity): array
    {
        $reflection = new ReflectionClass($entity::class);

        $props = $reflection->getProperties();

        $mappedProps = array_map(function (ReflectionProperty $p) use ($entity)
        {
            $changedAccess = false;
            if (!$p->isPublic())
            {
                $changedAccess = true;
                $p->setAccessible(true);
            }

            $result = [$p->getName() => $p->isInitialized($entity) ? $p->getValue($entity) : null];

            if ($changedAccess)
            {
                $p->setAccessible(false);
            }

            return $result;
        }, $props);

        return array_merge(...$mappedProps);
    }

    private function checkRequired(string $className, string $propName, $value): ValidationResultInterface
    {
        if (!$value || $value === null || empty($value) || !isset($value))
        {
            return new ValidationResult($className, $propName, "The {$propName} is required", ValidationResult::STATUS_ERROR);
        }

        return new ValidationResult($className, $propName);
    }

    private function checkMinLength(string $className, string $propName, string $value, int $minLength): ValidationResultInterface
    {
        return strlen($value) < $minLength
            ? new ValidationResult($className, $propName, "The {$propName} length less than {$minLength}", ValidationResult::STATUS_ERROR)
            : new ValidationResult($className, $propName);
    }

    private function checkMaxLength(string $className, string $propName, string $value, int $maxLength): ValidationResultInterface
    {
        return strlen($value) > $maxLength
            ? new ValidationResult($className, $propName, "The {$propName} length greater than {$maxLength}", ValidationResult::STATUS_ERROR)
            : new ValidationResult($className, $propName);
    }

    private function checkInRange(string $className, string $propName, int|float $value, int $from, int $to): ValidationResultInterface
    {
        return $value > $to && $value < $from
            ? new ValidationResult($className, $propName, "The {$propName} should be between {$from} and {$to}", ValidationResult::STATUS_ERROR)
            : new ValidationResult($className, $propName);
    }
}