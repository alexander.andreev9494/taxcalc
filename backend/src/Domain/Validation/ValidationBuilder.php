<?php


namespace App\Domain\Validation;


use Closure;
use Exception;
use App\Domain\Validation\Contracts\ValidationBuilderInterface;
use App\Domain\Validation\Contracts\ValidationRulesStageInterface;
use App\Domain\Validation\Contracts\CustomValidatorInterface;

class ValidationBuilder implements ValidationBuilderInterface, ValidationRulesStageInterface
{
    private array $properties = [];
    private string $propertyName = "";

    public function property(string $name): ValidationRulesStageInterface
    {
        $this->properties[$name] = [];
        $this->properties[$name]["required"] = false;
        $this->properties[$name]["minLength"] = false;
        $this->properties[$name]["maxLength"] = false;
        $this->properties[$name]["inRange"] = false;
        $this->properties[$name]["withCustomValidator"] = false;

        $this->propertyName = $name;

        return $this;
    }

    public function build(): array
    {
        return $this->properties;
    }

    public function isRequired(): ValidationRulesStageInterface
    {
        $this->properties[$this->propertyName]["required"] = true;
        return $this;
    }

    public function minLength(int $minLength): ValidationRulesStageInterface
    {
        $this->properties[$this->propertyName]["minLength"] = $minLength;
        return $this;
    }

    public function maxLength(int $maxLength): ValidationRulesStageInterface
    {
        $this->properties[$this->propertyName]["maxLength"] = $maxLength;
        return $this;
    }

    public function inRange(int|float $from, int|float $to): ValidationRulesStageInterface
    {
        $this->properties[$this->propertyName]["inRange"] = [$from, $to];
        return $this;
    }

    public function withCustomValidator(CustomValidatorInterface $validator): ValidationRulesStageInterface
    {
        if (!is_callable($validator))
        {
            throw new Exception("Provided object doesn't contain method __invoke(...)");
        }

        $this->properties[$this->propertyName]["withCustomValidator"][] = Closure::fromCallable($validator);
        return $this;
    }
}