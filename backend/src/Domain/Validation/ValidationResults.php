<?php


namespace App\Domain\Validation;


class ValidationResults
{
    public function __construct(private array $errors = [],
                                private array $warnings = [])
    {
    }

    /**
     * @return ValidationResult[]
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @return ValidationResult[]
     */
    public function getWarnings(): array
    {
        return $this->warnings;
    }

    public function hasErrors(): bool
    {
        return empty($this->errors) === false;
    }

    public function hasWarnings(): bool
    {
        return empty($this->warnings) === false;
    }
}