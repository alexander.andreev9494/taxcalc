<?php


namespace App\Domain\Validation;


use App\Domain\Validation\Contracts\ValidationBuilderInterface;
use App\Domain\Validation\Contracts\ValidationContextInterface;

class ValidationContainer
{
    public ?ValidationBuilderInterface $validationBuilder = null;
    public ?ValidationContextInterface $validationContext = null;
}