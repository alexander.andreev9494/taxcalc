<?php


namespace App\Domain\Validation;


use App\Domain\Validation\Contracts\ValidationResultInterface;

class ValidationResult implements ValidationResultInterface
{
    public function __construct(private string $className,
                                private string $propName,
                                private ?string $message = null,
                                private int $status = self::STATUS_OK)
    {

    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function getModelName(): string
    {
        return $this->className;
    }

    public function getPropertyName(): string
    {
        return $this->propName;
    }

    public function __toString(): string
    {
        return $this->message;
    }
}