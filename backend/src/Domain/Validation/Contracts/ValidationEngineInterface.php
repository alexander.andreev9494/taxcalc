<?php


namespace App\Domain\Validation\Contracts;


use App\Domain\Entities\BaseEntity;
use App\Domain\Validation\ValidationResults;
use Closure;

interface ValidationEngineInterface
{
    public function forEntity(string $entityClass, Closure $builder): void;
    public function validate(BaseEntity $entity): ValidationResults;
    public function withValidationContext(string $entityClass, ValidationContextInterface $validationContext): void;
}