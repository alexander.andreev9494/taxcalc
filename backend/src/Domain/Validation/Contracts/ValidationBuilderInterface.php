<?php


namespace App\Domain\Validation\Contracts;


interface ValidationBuilderInterface
{
    public function property(string $name): ValidationRulesStageInterface;
    public function build(): array;
}