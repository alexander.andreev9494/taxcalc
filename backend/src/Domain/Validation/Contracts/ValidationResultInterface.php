<?php


namespace App\Domain\Validation\Contracts;


interface ValidationResultInterface
{
    public const STATUS_ERROR = 1;
    public const STATUS_WARNING = 2;
    public const STATUS_OK = 3;

    public function getMessage(): ?string;
    public function getStatus(): int;
    public function getModelName(): string;
    public function getPropertyName(): string;
}