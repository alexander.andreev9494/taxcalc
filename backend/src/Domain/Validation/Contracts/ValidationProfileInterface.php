<?php


namespace App\Domain\Validation\Contracts;


interface ValidationProfileInterface
{
    public function register(): void;
}