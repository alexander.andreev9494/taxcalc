<?php


namespace App\Domain\Validation\Contracts;


interface ValidationProfileLoaderInterface
{
    public function load(): void;
}