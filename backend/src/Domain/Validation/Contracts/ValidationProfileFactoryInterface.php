<?php


namespace App\Domain\Validation\Contracts;


interface ValidationProfileFactoryInterface
{
    public function create(string $profileClass): ValidationProfileInterface;
}