<?php


namespace App\Domain\Validation\Contracts;


interface ValidationRulesStageInterface
{
    public function isRequired(): ValidationRulesStageInterface;
    public function minLength(int $minLength): ValidationRulesStageInterface;
    public function maxLength(int $maxLength): ValidationRulesStageInterface;
    public function inRange(int|float $from, int|float $to): ValidationRulesStageInterface;
    public function withCustomValidator(CustomValidatorInterface $validator): ValidationRulesStageInterface;
}