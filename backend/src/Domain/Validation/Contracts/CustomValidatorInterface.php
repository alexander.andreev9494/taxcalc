<?php


namespace App\Domain\Validation\Contracts;


interface CustomValidatorInterface
{
    //EXAMPLE:
    //validation context is a custom object which contains custom props

    /**
     *
     * public function __invoke(string $propName, string $value, TestEntityValidationContext $validationContext)
        {
            if (strlen($value) < $validationContext->warningLength)
            {
                return new ValidationResult(TestEntity::class, $propName);
            }
            else
            {
                return new ValidationResult(TestEntity::class, $propName, "Abnormal length", ValidationResult::STATUS_WARNING);
            }
        }
     */
}