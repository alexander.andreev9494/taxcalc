<?php


namespace App\Domain\Validation;


use Exception;

class ValidationException extends Exception
{
    /**
     * ValidationException constructor.
     * @param ValidationResult[] $validationResults
     */
    public function __construct(array $validationResults = [])
    {
        $message = "";

        foreach ($validationResults as $validationResult)
        {
            $message .= "{$validationResult->getMessage()}\n";
        }

        parent::__construct($message);
    }
}