<?php


namespace App\Domain\Persistence;


use App\Domain\Entities\BaseEntity;

interface DataManagerInterface
{
    public function getRepository(string $className): RepositoryInterface;
    public function persist(BaseEntity $baseEntity): void;
    public function remove(string $className, int $id): void;
    public function save(): void;
}