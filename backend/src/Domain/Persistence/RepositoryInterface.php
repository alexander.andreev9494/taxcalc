<?php


namespace App\Domain\Persistence;


use App\Domain\Entities\BaseEntity;

interface RepositoryInterface
{
    public function count(): int;
    public function findOne(int $id): ?BaseEntity;
    public function findBy(array $params = [], array $orderBy = []): array;
    public function findPaginated(int $page, int $pageSize): array;
    public function forDoropdown(string $keyName, string $valueName): array;
}