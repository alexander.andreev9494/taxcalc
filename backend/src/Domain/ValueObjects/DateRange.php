<?php


namespace App\Domain\ValueObjects;


use DateTime;
use DateInterval;
use Exception;
use DatePeriod;

class DateRange 
{
    public function __construct(private DateTime $from,
                                private DateTime $to)
    {
        if ($from > $to)
        {
            throw new Exception("'From' date can not be greater than 'to' date");
        }
    }

    public function getDistanceInDays(): int 
    {
        $interval = $this->diff();
        return $interval->d;
    }

    public function getDistanceInMonth(): int 
    {
        $interval = $this->diff();
        return $interval->m;
    }

    public function diff(): DateInterval
    {
        return $this->to->diff($this->from);
    }

    public function getFrom(): DateTime
    {
        return $this->from;
    }

    public function getTo(): DateTime
    {
        return $this->to;
    }

    public function getFirstMonth(): int
    {
        return (int)$this->from->format("m");
    }

    public function getLastMonth(): int
    {
        return (int)$this->to->format("m");
    }

    public function getPeriod(): DatePeriod
    {
        return new DatePeriod($this->from, new DateInterval("P1D"), $this->to);
    }
}