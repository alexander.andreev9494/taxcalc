<?php


namespace App\Domain\Mapping;


interface MapperInterface
{
    public function map(object $source, object $destination): void;
    public function mapByClass(string $destinationClass, object $source): object;
}