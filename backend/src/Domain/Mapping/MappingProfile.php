<?php 


namespace App\Domain\Mapping;


abstract class MappingProfile 
{
    private array $mappingConfigurations = [];

    public function getConfigurations(): array
    {
        return $this->mappingConfigurations;
    }

    public function getConfiguration(string $source, string $destination): ?MappingConfiguration
    {
        $configuration = null;
        $filteredConfigurations = array_filter($this->mappingConfigurations, fn (MappingConfiguration $c) => $c->getSourceClass() === $source && $c->getDestinationClass() === $destination);

        if (!empty($filteredConfigurations))
        {
            $configuration = current($filteredConfigurations);
        }
    
        return $configuration;
    }

    protected function createMap(string $source, string $destination): MappingConfiguration
    {
        $mappingConfiguration = null;
        if (!$this->exists($source, $destination))
        {
            $mappingConfiguration = new MappingConfiguration($source, $destination);
            $this->mappingConfigurations[] = $mappingConfiguration;
        } 
        else
        {
            $mappingConfiguration = $this->mappingConfigurations[$this->getConfigurationKey($source, $destination)];
        }
        
        return $mappingConfiguration;
    }

    private function exists(string $source, string $destination): bool
    {
        return $this->getConfigurationKey($source, $destination) !== -1;
    }

    private function getConfigurationKey(string $source, string $destination): int 
    {
        $key = -1;
        $result = array_filter($this->mappingConfigurations, fn (MappingConfiguration $c) => $c->getSourceClass() === $source && $c->getDestinationClass() === $destination);
        
        if (!empty($result))
        {
            $key = array_search($result[0], $this->mappingConfigurations);
        }

        return $key;
    }
}