<?php


namespace App\Domain\Mapping;


use ReflectionClass;

class Mapper implements MapperInterface
{
    private array $innerObjectProps = [];

    public function __construct(private MappingProfile $profile)
    {
        
    }

    public function map(object $source, object $destination): void
    {
        /** @var MappingConfiguration $configuration */
        $configuration = $this->profile->getConfiguration($source::class, $destination::class);
        
        $sourceReflection = new ReflectionClass($source::class);
        $destinationReflection = new ReflectionClass($destination::class);

        $sourceProps = $sourceReflection->getProperties();

        foreach ($sourceProps as $sourceProp)
        {
            $targetProp = null;
            $mapping = $configuration?->getMappingBySourcePropertyName($sourceProp->getName());
            if ($mapping === null)
            {
                if (!$destinationReflection->hasProperty($sourceProp->getName()))
                {
                    continue;
                }

                $targetProp = $destinationReflection->getProperty($sourceProp->getName());
            }
            else
            {
                $targetProp = $destinationReflection->getProperty($mapping->destination);
            }
            
            if ($targetProp === null)
            {
                continue;
            }

            $targetVisibilityChanged = false;
            if (!$targetProp->isPublic())
            {
                $targetProp->setAccessible(true);
                $targetVisibilityChanged = true;
            }

            $sourceVisibilityChanged = false;
            if (!$sourceProp->isPublic())
            {
                $sourceProp->setAccessible(true);
                $sourceVisibilityChanged = true;    
            }

            if (!$sourceProp->isInitialized($source))
            {
                continue;
            }

            $sourceValue = $sourceProp->getValue($source);

            if (is_object($sourceValue) && $mapping->customMapper === null)
            {
                $targetType = (string)$targetProp->getType();
                $reflection = new ReflectionClass($targetType);
                $mapTarget = $reflection->newInstanceWithoutConstructor();
                $this->map($sourceValue, $mapTarget);

                $sourceValue = $mapTarget;
            }
           
            if ($mapping?->customMapper !== null)
            {
                $mapper = $mapping->customMapper;
                if (class_exists((string)$targetProp->getType()))
                {
                    $obj = null;
                    $class = (string)$targetProp->getType();
                    
                    if (isset($this->innerObjectProps[$class]))
                    {
                        $obj = $this->innerObjectProps[$class];
                    }
                    else 
                    {
                        $obj = (new ReflectionClass($class))->newInstanceWithoutConstructor();
                        $this->innerObjectProps[$class] = $obj;
                    }
                    
                    $mapper($sourceValue, $obj);

                    $targetProp->setValue($destination, $obj); 
                }
                else
                {
                    $targetProp->setValue($destination, $mapper($sourceValue));
                }    
            }
            else
            {
                $targetProp->setValue($destination, $sourceValue);
            }

            if ($targetVisibilityChanged)
            {
                $targetProp->setAccessible(false);
            }

            if ($sourceVisibilityChanged)
            {
                $sourceProp->setAccessible(false);
            }
        }

        $this->innerObjectProps = [];
    }

    public function mapByClass(string $destinationClass, object $source): object
    {
        $targetReflection = new ReflectionClass($destinationClass);
        $targetObject = $targetReflection->newInstanceWithoutConstructor();

        $this->map($source, $targetObject);
        return $targetObject;
    }
}