<?php 


namespace App\Domain\Mapping;


use Closure;

class MappingConfiguration
{
    private array $mappingConfig;

    private string $source;
    private string $destination;

    public function __construct(string $sourceClass, string $destinationClass)
    {
        $this->mappingConfig = [];

        $this->source = $sourceClass;
        $this->destination = $destinationClass;
    }

    public function map(string $sourcePropName, string $destinationPropName, ?Closure $mapper = null): self
    {
        $this->mappingConfig[] = new Mapping($sourcePropName, $destinationPropName, $mapper);
        return $this;
    }

    public function getMappingBySourcePropertyName(string $sourcePropertyName): ?Mapping
    {
        $mapping = null;

        $result = array_filter($this->mappingConfig, fn (Mapping $m) => $m->source === $sourcePropertyName);
        if (!empty($result))
        {
            $mapping = current($result);
        }

        return $mapping;
    }

    public function getMappings(): array
    {
        return $this->mappingConfig;
    }

    public function getSourceClass(): string 
    {
        return $this->source;
    }

    public function getDestinationClass(): string 
    {
        return $this->destination;
    }
}