<?php 


namespace App\Domain\Mapping;


use Closure;

class Mapping
{
    public function __construct(public string $source, 
                                public string $destination,
                                public ?Closure $customMapper)
    {
        
    }
}