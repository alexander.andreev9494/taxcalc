<?php 


namespace App\Domain\Dto;

use App\Domain\ValueObjects\DateRange;

class TaxForPeriodDto
{
    public function __construct(public DateRange $period,
                                public float $totalIncome,
                                public float $totalTax,
                                public float $netRevenue)
    {
        
    }
}