<?php 


namespace App\Domain\Dto;

use DateTime;

class TaxForMonthDto
{
    public function __construct(public DateTime $taxDate,
                                public float $income,
                                public float $taxAmount)
    {
        
    }
}