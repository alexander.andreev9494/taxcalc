<?php


namespace App\Domain\Contracts;


interface SerializerInterface
{
    public function serialize(array|object $data): string;
    public function deserialize(string $className, string $data): array|object;
}