<?php


namespace App\Domain\Entities;


class AdditionalSpend extends BaseEntity
{
    public function __construct(public TaxProfile $profile,
                                public string $name,
                                public float $amount,
                                public bool $applyBeforeTaxation = true)
    {
    }
}