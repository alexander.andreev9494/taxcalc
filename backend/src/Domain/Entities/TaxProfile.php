<?php


namespace App\Domain\Entities;


use App\Domain\Dto\TaxForMonthDto;
use App\Domain\Dto\TaxForPeriodDto;
use App\Domain\ValueObjects\DateRange;
use DateTime;
use ArrayAccess;
use Countable;
use IteratorAggregate;

class TaxProfile extends BaseEntity
{
    public ArrayAccess|IteratorAggregate|Countable $taxes;
    public ArrayAccess|IteratorAggregate|Countable $incomes;
    public ArrayAccess|IteratorAggregate|Countable $additionalSpends;

    public function __construct(public string $name,
                                public string $description)
    {
    }

    public function calculateIncomeForDate(DateTime $date): float
    {
        $formattedDate = $date->format("Y-m-d");

        $incomeForMonth = 0.0;
        foreach ($this->incomes as $income)
        {
            $incomeDate = $income->getCreatedAt()->format("Y-m-d");
        
            if ($incomeDate === $formattedDate)
            {
                $incomeForMonth += $income->amount;
            }
        }

        return $this->applyAdditionalSpendsForIncomeBeforeTax($incomeForMonth);
    }

    public function calculateTax(float $income): float
    {
        $taxAmount = 0;
        foreach ($this->taxes as $tax)
        {
            if ($tax->isPercentage)
            {
                $taxAmount += $income * ($tax->amount / 100);
            } 
            else 
            {
                $taxAmount += $tax->amount;
            }
        }

        return $taxAmount;
    }

    public function calculateForPeriod(DateRange $range): array
    {
        $result = [];

        foreach ($range->getPeriod() as $dateTime)
        {
            $income = $this->calculateIncomeForDate($dateTime);
            $tax = $this->calculateTax($income);

            $result[] = new TaxForMonthDto($dateTime, $income, $tax);
        }

        return $result;
    }

    public function buildForPeriod(TaxForMonthDto ...$dtos): ?TaxForPeriodDto
    {
        if (empty($dtos))
        {
            return null;
        }

        $totalIncome = 0;
        $totalTax = 0;

        foreach ($dtos as $dto)
        {
            $totalIncome += $dto->income;
            $totalTax += $dto->taxAmount;
        }

        $from = new DateTime($dtos[0]->taxDate->format("Y")."-".$dtos[0]->taxDate->format("m")."-01");
        $to = new DateTime(end($dtos)->taxDate->format("Y")."-".end($dtos)->taxDate->format("m")."-01");

        $totalIncome = $this->applyAdditionalSpendsForIncomeAfterTax($totalIncome - $totalTax);

        return new TaxForPeriodDto(new DateRange($from, $to), $totalIncome, $totalTax, $totalIncome); 
    }

    private function applyAdditionalSpendsForIncomeBeforeTax(float $income): float
    {
        /** @var AdditionalSpend $additionalSpend */
        foreach ($this->additionalSpends as $additionalSpend)
        {
            $income -= $additionalSpend->applyBeforeTaxation ? $additionalSpend->amount : 0;
        }

        return $income;
    }

    private function applyAdditionalSpendsForIncomeAfterTax(float $income): float
    {
        /** @var AdditionalSpend $additionalSpend */
        foreach ($this->additionalSpends as $additionalSpend)
        {
            $income -= $additionalSpend->applyBeforeTaxation === false ? $additionalSpend->amount : 0;
        }

        return $income;
    }
}