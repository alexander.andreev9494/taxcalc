<?php


namespace App\Domain\Entities;


abstract class BaseEntity
{
    protected int $id;
    protected bool $deleted = false;

    public function getId(): int
    {
        return $this->id;
    }

    public function delete(): void
    {
        $this->deleted = true;
    }

}