<?php


namespace App\Domain\Entities;


class Tax extends BaseEntity
{
    public function __construct(public TaxProfile $profile,
                                public string $title,
                                public string $description,
                                public float $amount,
                                public bool $isPercentage,
                                public ?float $appliesBefore = null)
    {
    }
}