<?php


namespace App\Domain\Entities;


use DateTime;

class Income extends BaseEntity
{
    private DateTime $createdAt;

    public function __construct(public TaxProfile $taxProfile,
                                public float $amount)
    {
        $this->createdAt = new DateTime();
    }

    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

}