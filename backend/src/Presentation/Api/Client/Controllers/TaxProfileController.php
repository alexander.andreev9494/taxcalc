<?php 


namespace App\Presentation\Api\Client\Controllers;

use App\Application\Features\TaxProfile\Queries\GetReportForPeriodQuery;
use App\Domain\ValueObjects\DateRange;
use App\Infrastructure\Http\Controllers\ApiControllerBase;
use DateInterval;
use DateTime;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class TaxProfileController extends ApiControllerBase
{
    #[Route(path: "tax-profile/generate-report/{profileId}", methods: ["GET"])]
    public function generateReport(Request $request, int $profileId): JsonResponse
    {
        $from = new DateTime($request->get("from") ?? "now");
        $days = $request->get("days") ?? 30;
        $to = (new DateTime())->add(new DateInterval("P{$days}D"));

        $result = $this->queryBus->execute(new GetReportForPeriodQuery(new DateRange($from, $to), $profileId));
        return $this->ok($result);
    }
}