<?php


namespace App\Presentation\Api\Backoffice\Controllers;

use App\Application\Features\Tax\Commands\CreateTaxCommand;
use App\Application\Features\Tax\Commands\DeleteTaxCommand;
use App\Application\Features\Tax\Commands\UpdateTaxCommand;
use App\Application\Features\Tax\Queries\GetTaxByIdQuery;
use App\Application\Features\Tax\Queries\GetTaxesPaginatedQuery;
use App\Infrastructure\Http\Controllers\ApiControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class TaxController extends ApiControllerBase
{
    #[Route(path: "/tax/one/{id}", methods: ["GET"])]
    public function oneById(Request $request, int $id): JsonResponse 
    {
        $tax = $this->queryBus->execute(new GetTaxByIdQuery($id));
        return $this->ok($tax);
    }

    #[Route(path: "/tax", methods: ["GET"])]
    public function paginated(Request $request): JsonResponse
    {
        $taxes = $this->queryBus->execute(new GetTaxesPaginatedQuery($this->getPage(), $this->getPageSize()));
        return $this->ok($taxes);
    }

    #[Route(path: "/tax/create", methods: ["POST"])]
    public function create(Request $request): JsonResponse
    {
        $command = $this->mapRequestBody(CreateTaxCommand::class);
        $result = $this->commandBus->dispatch($command);
        if (!$result->isSuccess())
        {
            return $this->badRequest($result->getMessages());
        }

        return $this->created($result->getMessages());
    }

    #[Route(path: "/tax/update", methods: ["PUT"])]
    public function update(): JsonResponse
    {
        $command = $this->mapRequestBody(UpdateTaxCommand::class);
        $result = $this->commandBus->dispatch($command);
        if (!$result->isSuccess())
        {
            return $this->badRequest($result->getMessages());
        }

        return $this->ok($result->getMessages());
    }

    #[Route(path: "/tax/delete/{id}", methods: ["DELETE"])]
    public function delete(Request $request, int $id): JsonResponse
    {
        $this->commandBus->dispatch(new DeleteTaxCommand($id));
        return $this->ok();
    }
}