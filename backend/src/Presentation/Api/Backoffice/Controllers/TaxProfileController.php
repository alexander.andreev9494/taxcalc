<?php


namespace App\Presentation\Api\Backoffice\Controllers;


use App\Application\Features\TaxProfile\Commands\CreateTaxProfileCommand;
use App\Application\Features\TaxProfile\Commands\DeleteProfileCommand;
use App\Application\Features\TaxProfile\Commands\UpdateProfileCommand;
use App\Application\Features\TaxProfile\Queries\GetProfileByIdQuery;
use App\Application\Features\TaxProfile\Queries\GetProfilesForDropdownQuery;
use App\Application\Features\TaxProfile\Queries\GetProfilesPaginatedQuery;
use App\Domain\Cqrs\PaginatedQueryResult;
use App\Infrastructure\Http\Controllers\ApiControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class TaxProfileController extends ApiControllerBase
{
    #[Route(path: "/tax-profile", methods: ["GET"])]
    public function paginated(Request $request): JsonResponse
    {
        /** @var PaginatedQueryResult $result */
        $result = $this->queryBus->execute(new GetProfilesPaginatedQuery($this->getPage(), $this->getPageSize()));
        return $this->ok($result);
    }

    #[Route(path: "/tax-profile/one/{id}", methods: ["GET"])]
    public function getOne(Request $request, int $id): JsonResponse
    {
        $profile = $this->queryBus->execute(new GetProfileByIdQuery($id));
        return $this->ok($profile);
    }

    #[Route(path: "/tax-profile/for-dropdown", methods: ["GET"])]
    public function forDropDown(Request $request): JsonResponse
    {
        $profilesForDropdown = $this->queryBus->execute(new GetProfilesForDropdownQuery());
        return $this->ok($profilesForDropdown);
    }

    #[Route(path: "/tax-profile/create", methods: ["POST"])]
    public function create(Request $request): JsonResponse
    {
        $command = $this->mapRequestBody(CreateTaxProfileCommand::class);
        $result = $this->commandBus->dispatch($command);
        if (!$result->isSuccess())
        {
            return $this->badRequest($result->getMessages());
        }

        return $this->created($result->getMessages());
    }

    #[Route(path: "/tax-profile/update", methods: ["PUT"])]
    public function update(Request $request): JsonResponse
    {
        $command = $this->mapRequestBody(UpdateProfileCommand::class);
        $result = $this->commandBus->dispatch($command);
        if (!$result->isSuccess())
        {
            return $this->badRequest($result->getMessages());
        }

        return $this->ok($result->getMessages());
    }

    #[Route(path: "/tax-profile/delete/{id}", methods: ["DELETE"])]
    public function delete(Request $request, int $id): JsonResponse
    {
        $this->commandBus->dispatch(new DeleteProfileCommand($id));
        return $this->ok();
    }
}