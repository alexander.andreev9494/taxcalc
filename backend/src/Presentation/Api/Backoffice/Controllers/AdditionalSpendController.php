<?php 


namespace App\Presentation\Api\Backoffice\Controllers;

use App\Application\Features\AdditionalSpend\Commands\CreateAdditionalSpendCommand;
use App\Application\Features\AdditionalSpend\Commands\DeleteAdditionalSpendCommand;
use App\Application\Features\AdditionalSpend\Commands\UpdateAdditionalSpendCommand;
use App\Application\Features\AdditionalSpend\Queries\GetAdditionalSpendByIdQuery;
use App\Application\Features\AdditionalSpend\Queries\GetAdditionalSpendsPaginatedQuery;
use App\Infrastructure\Http\Controllers\ApiControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdditionalSpendController extends ApiControllerBase
{
    #[Route(path: "/additional-spend/one/{id}", methods: ["GET"])]
    public function oneById(Request $request, int $id): JsonResponse 
    {
        $spend = $this->queryBus->execute(new GetAdditionalSpendByIdQuery($id));
        return $this->ok($spend);
    }

    #[Route(path: "/additional-spend", methods: ["GET"])]
    public function paginated(Request $request): JsonResponse
    {
        $spends = $this->queryBus->execute(new GetAdditionalSpendsPaginatedQuery($this->getPage(), $this->getPageSize()));
        return $this->ok($spends);
    }

    #[Route(path: "/additional-spend/create", methods: ["POST"])]
    public function create(Request $request): JsonResponse
    {
        $command = $this->mapRequestBody(CreateAdditionalSpendCommand::class);
        $result = $this->commandBus->dispatch($command);
        if (!$result->isSuccess())
        {
            return $this->badRequest($result->getMessages());
        }

        return $this->created($result->getMessages());
    }

    #[Route(path: "/additional-spend/update", methods: ["PUT"])]
    public function update(): JsonResponse
    {
        $command = $this->mapRequestBody(UpdateAdditionalSpendCommand::class);
        $result = $this->commandBus->dispatch($command);
        if (!$result->isSuccess())
        {
            return $this->badRequest($result->getMessages());
        }

        return $this->ok($result->getMessages());
    }

    #[Route(path: "/additional-spend/delete/{id}", methods: ["DELETE"])]
    public function delete(Request $request, int $id): JsonResponse
    {
        $this->commandBus->dispatch(new DeleteAdditionalSpendCommand($id));
        return $this->ok();
    }
}