<?php


namespace App\Presentation\Api\Backoffice\Controllers;


use App\Application\Features\Income\Commands\CreateIncomeCommand;
use App\Application\Features\Income\Commands\DeleteIncomeCommand;
use App\Application\Features\Income\Commands\UpdateIncomeCommand;
use App\Application\Features\Income\Queries\GetIncomeByIdQuery;
use App\Application\Features\Income\Queries\GetIncomePaginatedQuery;
use App\Infrastructure\Http\Controllers\ApiControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class IncomeController extends ApiControllerBase
{
    #[Route(path: "/income/one/{id}", methods: ["GET"])]
    public function oneById(Request $request, int $id): JsonResponse 
    {
        $$income = $this->queryBus->execute(new GetIncomeByIdQuery($id));
        return $this->ok($tax);
    }

    #[Route(path: "/income", methods: ["GET"])]
    public function paginated(Request $request): JsonResponse
    {
        $incomes = $this->queryBus->execute(new GetIncomePaginatedQuery($this->getPage(), $this->getPageSize()));
        return $this->ok($incomes);
    }

    #[Route(path: "/income/create", methods: ["POST"])]
    public function create(Request $request): JsonResponse
    {
        $command = $this->mapRequestBody(CreateIncomeCommand::class);
        $result = $this->commandBus->dispatch($command);
        if (!$result->isSuccess())
        {
            return $this->badRequest($result->getMessages());
        }

        return $this->created($result->getMessages());
    }

    #[Route(path: "/income/update", methods: ["PUT"])]
    public function update(): JsonResponse
    {
        $command = $this->mapRequestBody(UpdateIncomeCommand::class);
        $result = $this->commandBus->dispatch($command);
        if (!$result->isSuccess())
        {
            return $this->badRequest($result->getMessages());
        }

        return $this->ok($result->getMessages());
    }

    #[Route(path: "/income/delete/{id}", methods: ["DELETE"])]
    public function delete(Request $request, int $id): JsonResponse
    {
        $this->commandBus->dispatch(new DeleteIncomeCommand($id));
        return $this->ok();
    }
}