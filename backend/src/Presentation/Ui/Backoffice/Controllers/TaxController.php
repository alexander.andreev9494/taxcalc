<?php


namespace App\Presentation\Ui\Backoffice\Controllers;


use App\Application\Features\Tax\Commands\CreateTaxCommand;
use App\Application\Features\Tax\Commands\DeleteTaxCommand;
use App\Application\Features\Tax\Commands\UpdateTaxCommand;
use App\Application\Features\Tax\Queries\GetTaxByIdQuery;
use App\Application\Features\Tax\Queries\GetTaxesPaginatedQuery;
use App\Domain\Cqrs\PaginatedQueryResult;
use App\Infrastructure\Http\Controllers\UiControllerBase;
use App\Presentation\Ui\Backoffice\Forms\TaxCreateForm;
use App\Presentation\Ui\Backoffice\Forms\TaxEditForm;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TaxController extends UiControllerBase
{
    #[Route(path: "tax/index", name: "tax_index", methods: ["GET"])]
    public function index(Request $request): Response
    {
        /** @var PaginatedQueryResult $taxes */
        $taxes = $this->queryBus->execute(new GetTaxesPaginatedQuery($this->getPage(), $this->getPageSize()));
        return $this->render("@backoffice/tax/index.html.twig", ["taxes" => $taxes->data]);
    }

    #[Route(path: "tax/create", name: "tax_create", methods: ["GET", "POST"])]
    public function create(Request $request): Response
    {
        $form = $this->createForm(TaxCreateForm::class, new CreateTaxCommand());
        $form->handleRequest($request);

        if ($form->isSubmitted())
        {
            $command = $form->getData();
            $result = $this->commandBus->dispatch($command);
            if (!$this->setValidationResults($result, $form))
            {
                return $this->redirectToRoute("tax_index");
            }
        }

        return $this->render("@backoffice/tax/create.html.twig", ["form" => $form->createView()]);
    }

    #[Route(path: "tax/edit/{id}", name: "tax_edit", methods: ["GET", "POST"])]
    public function edit(Request $request, int $id): Response
    {
        $tax = $this->queryBus->execute(new GetTaxByIdQuery($id));

        $form = $this->createForm(TaxEditForm::class, $this->mapper->mapByClass(UpdateTaxCommand::class, $tax));
        $form->handleRequest($request);

        if ($form->isSubmitted())
        {
            $command = $form->getData();
            $result = $this->commandBus->dispatch($command);
            if (!$this->setValidationResults($result, $form))
            {
                return $this->redirectToRoute("tax_index");
            }
        }

        return $this->render("@backoffice/tax/edit.html.twig", ["form" => $form->createView()]);
    }

    #[Route(path: "tax/delete/{id}", name: "tax_delete", methods: ["GET"])]
    public function delete(Request $request, int $id): Response
    {
        $this->commandBus->dispatch(new DeleteTaxCommand($id));
        return $this->redirectToRoute("tax_index");
    }
}