<?php 


namespace App\Presentation\Ui\Backoffice\Controllers;

use App\Application\Features\Income\Commands\CreateIncomeCommand;
use App\Application\Features\Income\Commands\DeleteIncomeCommand;
use App\Application\Features\Income\Commands\UpdateIncomeCommand;
use App\Application\Features\Income\Queries\GetIncomeByIdQuery;
use App\Application\Features\Income\Queries\GetIncomePaginatedQuery;
use App\Infrastructure\Http\Controllers\UiControllerBase;
use App\Presentation\Ui\Backoffice\Forms\IncomeCreateForm;
use App\Presentation\Ui\Backoffice\Forms\IncomeEditForm;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IncomeController extends UiControllerBase
{
    #[Route(path: "income/index", name: "income_index", methods: ["GET"])]
    public function index(Request $request): Response
    {
        $incomes = $this->queryBus->execute(new GetIncomePaginatedQuery($this->getPage(), $this->getPageSize()));
        return $this->render("@backoffice/income/index.html.twig", ["incomes" => $incomes->data]);
    }

    #[Route(path: "income/create", name: "income_create", methods: ["GET", "POST"])]
    public function create(Request $request): Response
    {
        $form = $this->createForm(IncomeCreateForm::class, new CreateIncomeCommand());
        $form->handleRequest($request);

        if ($form->isSubmitted())
        {
            $command = $form->getData();
            $result = $this->commandBus->dispatch($command);
            if (!$this->setValidationResults($result, $form))
            {
                return $this->redirectToRoute("income_index");
            }
        }

        return $this->render("@backoffice/income/create.html.twig", ["form" => $form->createView()]);
    }

    #[Route(path: "income/edit/{id}", name: "income_edit", methods: ["GET", "POST"])]
    public function edit(Request $request, int $id): Response
    {
        $income = $this->queryBus->execute(new GetIncomeByIdQuery($id));

        $form = $this->createForm(IncomeEditForm::class, $this->mapper->mapByClass(UpdateIncomeCommand::class, $income));
        $form->handleRequest($request);

        if ($form->isSubmitted())
        {
            $command = $form->getData();
            $result = $this->commandBus->dispatch($command);
            if (!$this->setValidationResults($result, $form))
            {
                return $this->redirectToRoute("income_index");
            }
        }

        return $this->render("@backoffice/income/edit.html.twig", ["form" => $form->createView()]);
    }

    #[Route(path: "income/delete/{id}", name: "income_delete", methods: ["GET"])]
    public function delete(Request $request, int $id): Response
    {
        $this->commandBus->dispatch(new DeleteIncomeCommand($id));
        return $this->redirectToRoute("income_index");
    }
}