<?php


namespace App\Presentation\Ui\Backoffice\Controllers;


use App\Application\Features\TaxProfile\Commands\CreateTaxProfileCommand;
use App\Application\Features\TaxProfile\Commands\DeleteProfileCommand;
use App\Application\Features\TaxProfile\Commands\UpdateProfileCommand;
use App\Application\Features\TaxProfile\Queries\GetProfileByIdQuery;
use App\Application\Features\TaxProfile\Queries\GetProfilesPaginatedQuery;
use App\Domain\Cqrs\PaginatedQueryResult;
use App\Infrastructure\Http\Controllers\UiControllerBase;
use App\Presentation\Ui\Backoffice\Forms\TaxProfileCreateForm;
use App\Presentation\Ui\Backoffice\Forms\TaxProfileEditForm;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TaxProfileController extends UiControllerBase
{
    #[Route(path: "tax-profile/index", name: "tax_profile_index", methods: ["GET"])]
    public function index(Request $request): Response
    {
        /** @var PaginatedQueryResult $taxProfiles */
        $taxProfiles = $this->queryBus->execute(new GetProfilesPaginatedQuery($this->getPage(), $this->getPageSize()));
        return $this->render("@backoffice/taxProfile/index.html.twig", ["taxProfiles" => $taxProfiles->data]);
    }

    #[Route(path: "tax-profile/create", name: "tax_profile_create", methods: ["GET", "POST"])]
    public function create(Request $request): Response
    {
        $form = $this->createForm(TaxProfileCreateForm::class, new CreateTaxProfileCommand());
        $form->handleRequest($request);

        if ($form->isSubmitted())
        {
            $command = $form->getData();
            $result = $this->commandBus->dispatch($command);
            if (!$this->setValidationResults($result, $form))
            {
                return $this->redirectToRoute("tax_profile_index");
            }
        }

        return $this->render("@backoffice/taxProfile/create.html.twig", ["form" => $form->createView()]);
    }

    #[Route(path: "tax-profile/edit/{id}", name: "tax_profile_edit", methods: ["GET", "POST"])]
    public function edit(Request $request, int $id): Response 
    {
        $taxProfile = $this->queryBus->execute(new GetProfileByIdQuery($id));

        $form = $this->createForm(TaxProfileEditForm::class, $this->mapper->mapByClass(UpdateProfileCommand::class, $taxProfile));
        $form->handleRequest($request);

        if ($form->isSubmitted())
        {
            $command = $form->getData();
            $result = $this->commandBus->dispatch($command);
            if (!$this->setValidationResults($result, $form))
            {
                return $this->redirectToRoute("tax_profile_index");
            }
        }

        return $this->render("@backoffice/taxProfile/create.html.twig", ["form" => $form->createView()]);
    }

    #[Route(path: "tax-profile/delete/{id}", name: "tax_profile_delete", methods: ["GET"])]
    public function delete(Request $request, int $id): Response
    {
        $this->commandBus->dispatch(new DeleteProfileCommand($id));
        return $this->redirectToRoute("tax_profile_index");
    }
}