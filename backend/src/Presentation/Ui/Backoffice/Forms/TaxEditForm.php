<?php 


namespace App\Presentation\Ui\Backoffice\Forms;


use App\Application\Features\Tax\Commands\UpdateTaxCommand;
use App\Application\Features\TaxProfile\Queries\GetProfilesForDropdownQuery;
use App\Domain\Cqrs\Contracts\QueryBusInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TaxEditForm extends AbstractType
{
    public function __construct(private QueryBusInterface $queryBus)
    {
        
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $profiles = $this->queryBus->execute(new GetProfilesForDropdownQuery());

        $builder->add("title", TextType::class)
                ->add("description", TextareaType::class)
                ->add("amount", TextType::class)
                ->add("isPercentage", CheckboxType::class)
                ->add("appliesBefore", TextType::class, ["required" => false])
                ->add("profileId", ChoiceType::class, 
                    [
                        "placeholder" => "Select Tax Profile",
                        "choices" => $profiles,
                    ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(["data_class" => UpdateTaxCommand::class]);
    }
}