<?php 


namespace App\Presentation\Ui\Backoffice\Forms;


use App\Application\Features\Income\Commands\UpdateIncomeCommand;
use App\Application\Features\TaxProfile\Queries\GetProfilesForDropdownQuery;
use App\Domain\Cqrs\Contracts\QueryBusInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class IncomeEditForm extends AbstractType
{
    public function __construct(private QueryBusInterface $queryBus)
    {
        
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $profiles = $this->queryBus->execute(new GetProfilesForDropdownQuery());

        $builder->add("amount", TextType::class)
                ->add("profileId", ChoiceType::class, [
                    "placeholder" => "Select Tax Profile",
                    "choices" => $profiles
                ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(["data_class" => UpdateIncomeCommand::class]);
    }
}