<?php


namespace App\Application\Abstractions;


use App\Domain\Cqrs\Contracts\HandlerInterface;
use App\Domain\Entities\BaseEntity;
use App\Domain\Mapping\MapperInterface;
use App\Domain\Persistence\DataManagerInterface;
use App\Domain\Validation\Contracts\ValidationContextInterface;
use App\Domain\Validation\Contracts\ValidationEngineInterface;
use App\Domain\Validation\Contracts\ValidationProfileLoaderInterface;
use App\Domain\Validation\ValidationResults;

abstract class BaseCommandHandler implements HandlerInterface
{
    public function __construct(protected DataManagerInterface $dataManager,
                                protected ValidationEngineInterface $validationEngine,
                                protected MapperInterface $mapper,
                                ValidationProfileLoaderInterface $validationProfileLoader)
    {
        $validationProfileLoader->load();
    }

    protected function runValidation(BaseEntity $entity, ?ValidationContextInterface $validationContext = null): ValidationResults
    {
        if ($validationContext !== null)
        {
            $this->validationEngine->withValidationContext($entity::class, $validationContext);
        }
        
        return $this->validationEngine->validate($entity);
    }
}