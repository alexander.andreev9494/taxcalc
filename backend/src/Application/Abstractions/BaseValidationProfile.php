<?php


namespace App\Application\Abstractions;


use App\Domain\Validation\Contracts\ValidationEngineInterface;
use App\Domain\Validation\Contracts\ValidationProfileInterface;

abstract class BaseValidationProfile implements ValidationProfileInterface
{
    public function __construct(protected ValidationEngineInterface $validationEngine)
    {
    }
}