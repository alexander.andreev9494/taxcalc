<?php 


namespace App\Application\Abstractions;


use App\Domain\Cqrs\Contracts\HandlerInterface;
use App\Domain\Persistence\DataManagerInterface;

abstract class BaseQueryHandler implements HandlerInterface
{
    public function __construct(protected DataManagerInterface $dataManager)
    {
        
    }
}