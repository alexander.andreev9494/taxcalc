<?php 


namespace App\Application\Mapping;

use App\Application\Features\Income\Commands\UpdateIncomeCommand;
use App\Application\Features\Tax\Commands\UpdateTaxCommand;
use App\Domain\Cqrs\Contracts\QueryBusInterface;
use App\Domain\Entities\Income;
use App\Domain\Entities\Tax;
use App\Domain\Entities\TaxProfile;
use App\Domain\Mapping\MappingProfile;

class AppMappingProfile extends MappingProfile
{
    public function __construct(public QueryBusInterface $queryBus)
    {
        $this->createMap(Tax::class, UpdateTaxCommand::class)
            ->map("profile", "profileId", fn (TaxProfile $p) => $p->getId());

        $this->createMap(Income::class, UpdateIncomeCommand::class)
            ->map("taxProfile", "profileId", fn (TaxProfile $p) => $p->getId());

        /* $this->createMap(CreateIncomeCommand::class, Income::class)
            ->map("profileId", "taxProfile", 
                    fn (int $id) => $this->queryBus->execute(new GetProfileByIdQuery($id))); */
    }
}