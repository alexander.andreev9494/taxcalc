<?php

namespace App\Application\Metadata\Services;

use App\Domain\Metadata\PropertyDescriptor;
use ReflectionClass;
use ReflectionProperty;
use ReflectionAttribute;
use ReflectionParameter;
use App\Domain\Metadata\ClassDescriptor;
use App\Domain\Metadata\Services\MetadataServiceInterface;

class MetadataService implements MetadataServiceInterface
{
    private const CACHE_METADATA_KEY = "class_metadata";
    
    public function getMetadataByClass(string $className): ?ClassDescriptor
    {
        $descriptor = null;
        if (apcu_exists(self::CACHE_METADATA_KEY))
        {
            /** @var ClassDescriptor[] $metadata */
            $metadata = (array) apcu_fetch(self::CACHE_METADATA_KEY);

            if (isset($metadata[$className]))
            {
                $descriptor = $metadata[$className];
            }
        }

        return $descriptor;
    }

    public function registerMetadataForClass(string $className): void
    {
        if (!apcu_exists(self::CACHE_METADATA_KEY))
        {
            apcu_store(self::CACHE_METADATA_KEY, []);
        }

        $metadata = (array) apcu_fetch(self::CACHE_METADATA_KEY);

        if (!isset($metadata[$className]))
        {
            $reflection = new ReflectionClass($className);

            $properties = $this->loadProperties($reflection);
            $attributes = $this->loadAttributes($reflection);
            $constructorParameters = $this->loadConstructorParameters($reflection);
            $parentClass = $this->loadParentClass($reflection);
            $interfaces = $this->loadInterfaces($reflection);

            $descriptor = new ClassDescriptor($className, $attributes, $properties, $constructorParameters, $parentClass, $interfaces);

            $metadata[$className] = $descriptor;

            apcu_store(self::CACHE_METADATA_KEY, $metadata);
        }
    }

    public function registerMetadataForClasses(array $classes): void
    {
        foreach ($classes as $class)
        {
            $this->registerMetadataForClass($class);
        }
    }

    private function loadProperties(ReflectionClass $reflectionClass): array
    {
        return array_map(fn (ReflectionProperty $property) => new PropertyDescriptor($property->getName(),
            $property->getType(),
            array_map(fn (ReflectionAttribute $attribute) => $attribute->newInstance(), $property->getAttributes()),
            $property->isPublic()
        ), $reflectionClass->getProperties());
    }

    private function loadAttributes(ReflectionClass $reflectionClass): array
    {
        $reflectionAttributes = $reflectionClass->getAttributes();

        return array_map(fn (ReflectionAttribute $attribute) => $attribute->newInstance(), $reflectionAttributes);
    }

    private function loadConstructorParameters(ReflectionClass $reflectionClass): array
    {
        $parameters = [];

        $constructor = $reflectionClass->getConstructor();
        if ($constructor !== null)
        {
            $parameters = array_map(fn (ReflectionParameter $parameter) => $parameter->getType()->getName(), $constructor->getParameters());
        }

        return $parameters;
    }

    public function loadParentClass(ReflectionClass $reflectionClass): ?string
    {
        $parent = $reflectionClass->getParentClass();
        return !$parent ? null : $parent->getName();
    }

    private function loadInterfaces(ReflectionClass $reflectionClass): array
    {
        $interfaces = $reflectionClass->getInterfaces();
        return array_map(fn(ReflectionClass $reflectionClass) => $reflectionClass->getName(), $interfaces);
    }
}