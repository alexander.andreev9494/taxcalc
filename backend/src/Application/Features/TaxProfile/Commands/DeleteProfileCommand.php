<?php


namespace App\Application\Features\TaxProfile\Commands;


use App\Domain\Cqrs\Attributes\Handler;
use App\Domain\Cqrs\Contracts\CommandInterface;

#[Handler(DeleteProfileCommandHandler::class)]
class DeleteProfileCommand implements CommandInterface
{
    public function __construct(public int $id)
    {
    }
}