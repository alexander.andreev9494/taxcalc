<?php


namespace App\Application\Features\TaxProfile\Commands;


use App\Domain\Cqrs\Attributes\Handler;
use App\Domain\Cqrs\Contracts\CommandInterface;

#[Handler(CreateTaxProfileCommandHandler::class)]
class CreateTaxProfileCommand implements CommandInterface
{
    public string $name;
    public string $description;
}