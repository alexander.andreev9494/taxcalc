<?php


namespace App\Application\Features\TaxProfile\Commands;


use App\Application\Abstractions\BaseCommandHandler;
use App\Application\Features\TaxProfile\Validation\TaxProfileValidationContext;
use App\Domain\Cqrs\CommandResult;
use App\Domain\Entities\TaxProfile;

class UpdateProfileCommandHandler extends BaseCommandHandler
{
    public function __invoke(UpdateProfileCommand $command): CommandResult
    {
        /** @var TaxProfile $profile */
        $profile = $this->dataManager->getRepository(TaxProfile::class)
            ->findOne($command->id);

        $this->mapper->map($command, $profile);

        $validationResults = $this->runValidation($profile, new TaxProfileValidationContext($profile->getId()));

        if (!$validationResults->hasErrors())
        {
            $this->dataManager->persist($profile);
            $this->dataManager->save();

            return CommandResult::ok($validationResults->getWarnings());
        }

        return CommandResult::fail($validationResults->getErrors());
    }
}