<?php


namespace App\Application\Features\TaxProfile\Commands;


use App\Application\Abstractions\BaseCommandHandler;
use App\Domain\Cqrs\CommandResult;
use App\Domain\Entities\TaxProfile;

class CreateTaxProfileCommandHandler extends BaseCommandHandler
{
    public function __invoke(CreateTaxProfileCommand $command): CommandResult
    {
        /** @var TaxProfile $profile */
        $profile = $this->mapper->mapByClass(TaxProfile::class, $command);

        $validationResults = $this->runValidation($profile);

        if (!$validationResults->hasErrors())
        {
            $this->dataManager->persist($profile);
            $this->dataManager->save();

            return CommandResult::ok(["id" => $profile->getId(), "warnings" => $validationResults->getWarnings()]);
        }

        return CommandResult::fail($validationResults->getErrors());
    }
}