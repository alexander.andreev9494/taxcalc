<?php


namespace App\Application\Features\TaxProfile\Commands;


use App\Domain\Cqrs\Attributes\Handler;
use App\Domain\Cqrs\Contracts\CommandInterface;

#[Handler(UpdateProfileCommandHandler::class)]
class UpdateProfileCommand implements CommandInterface
{
    public function __construct(public int $id,
                                public ?string $name = null,
                                public ?string $description = null)
    {
    }
}