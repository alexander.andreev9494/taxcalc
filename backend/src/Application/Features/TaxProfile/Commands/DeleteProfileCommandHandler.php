<?php


namespace App\Application\Features\TaxProfile\Commands;


use App\Application\Abstractions\BaseCommandHandler;
use App\Domain\Cqrs\CommandResult;
use App\Domain\Entities\TaxProfile;

class DeleteProfileCommandHandler extends BaseCommandHandler
{
    public function __invoke(DeleteProfileCommand $command): CommandResult
    {
        $this->dataManager->remove(TaxProfile::class, $command->id);
        $this->dataManager->save();

        return CommandResult::ok();
    }
}