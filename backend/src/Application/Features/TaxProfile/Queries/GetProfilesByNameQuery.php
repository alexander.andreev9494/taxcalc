<?php 


namespace App\Application\Features\TaxProfile\Queries;

use App\Domain\Cqrs\Attributes\Handler;
use App\Domain\Cqrs\Contracts\QueryInterface;

#[Handler(GetProfilesByNameQueryHandler::class)]
class GetProfilesByNameQuery implements QueryInterface
{
    public function __construct(public string $profileName)
    {
        
    }
}