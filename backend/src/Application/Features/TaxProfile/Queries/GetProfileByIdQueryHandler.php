<?php


namespace App\Application\Features\TaxProfile\Queries;


use App\Application\Abstractions\BaseQueryHandler;
use App\Domain\Entities\TaxProfile;

class GetProfileByIdQueryHandler extends BaseQueryHandler
{
    public function __invoke(GetProfileByIdQuery $query): ?TaxProfile
    {
        return $this->dataManager->getRepository(TaxProfile::class)
            ->findOne($query->id);
    }
}