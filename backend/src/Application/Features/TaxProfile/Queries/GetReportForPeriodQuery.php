<?php 


namespace App\Application\Features\TaxProfile\Queries;

use App\Domain\Cqrs\Attributes\Handler;
use App\Domain\Cqrs\Contracts\QueryInterface;
use App\Domain\ValueObjects\DateRange;

#[Handler(GetReportForPeriodQueryHandler::class)]
class GetReportForPeriodQuery implements QueryInterface
{
    public function __construct(public DateRange $period,
                                public int $profileId)
    {
        
    }
}