<?php 


namespace App\Application\Features\TaxProfile\Queries;

use App\Domain\Cqrs\Attributes\Handler;
use App\Domain\Cqrs\Contracts\QueryInterface;


#[Handler(GetProfilesForDropdownQueryHandler::class)]
class GetProfilesForDropdownQuery implements QueryInterface
{

}