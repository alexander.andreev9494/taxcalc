<?php 


namespace App\Application\Features\TaxProfile\Queries;


use App\Application\Abstractions\BaseQueryHandler;
use App\Domain\Entities\TaxProfile;

class GetProfilesForDropdownQueryHandler extends BaseQueryHandler
{
    public function __invoke(GetProfilesForDropdownQuery $query): array
    {
        return $this->dataManager->getRepository(TaxProfile::class)->forDoropdown("id", "name");
    }
}