<?php 


namespace App\Application\Features\TaxProfile\Queries;


use App\Application\Abstractions\BaseQueryHandler;
use App\Domain\Dto\TaxForPeriodDto;
use App\Domain\Entities\TaxProfile;
use Exception;

class GetReportForPeriodQueryHandler extends BaseQueryHandler
{
    public function __invoke(GetReportForPeriodQuery $query): ?TaxForPeriodDto
    {
        /** @var TaxProfile $profile */
        $profile = $this->dataManager->getRepository(TaxProfile::class)->findOne($query->profileId);
        if ($profile === null)
        {
            throw new Exception("Can not get profile by id: {$query->profileId}");
        }

        $data = $profile->calculateForPeriod($query->period);
        $result = $profile->buildForPeriod(...$data);
        return $result;
    }
}