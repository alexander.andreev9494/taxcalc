<?php


namespace App\Application\Features\TaxProfile\Queries;


use App\Domain\Cqrs\Attributes\Handler;
use App\Domain\Cqrs\Contracts\QueryInterface;

#[Handler(GetProfilesPaginatedQueryHandler::class)]
class GetProfilesPaginatedQuery implements QueryInterface
{
    public function __construct(public int $pageNum,
                                public int $pageSize)
    {
    }
}