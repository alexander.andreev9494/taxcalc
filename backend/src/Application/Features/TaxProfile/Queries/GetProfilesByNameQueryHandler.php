<?php 


namespace App\Application\Features\TaxProfile\Queries;


use App\Application\Abstractions\BaseQueryHandler;
use App\Domain\Entities\TaxProfile;

class GetProfilesByNameQueryHandler extends BaseQueryHandler
{
    public function __invoke(GetProfilesByNameQuery $query): array
    {
        $result = $this->dataManager->getRepository(TaxProfile::class)
                    ->findBy(["name" => $query->profileName]);
                    
        return $result;
    }
}