<?php


namespace App\Application\Features\TaxProfile\Queries;


use App\Domain\Cqrs\Attributes\Handler;
use App\Domain\Cqrs\Contracts\QueryInterface;

#[Handler(GetProfileByIdQueryHandler::class)]
class GetProfileByIdQuery implements QueryInterface
{
    public function __construct(public int $id)
    {
    }
}