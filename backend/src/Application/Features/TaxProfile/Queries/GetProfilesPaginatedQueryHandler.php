<?php


namespace App\Application\Features\TaxProfile\Queries;

use App\Application\Abstractions\BaseQueryHandler;
use App\Domain\Cqrs\PaginatedQueryResult;
use App\Domain\Entities\TaxProfile;

class GetProfilesPaginatedQueryHandler extends BaseQueryHandler
{
    public function __invoke(GetProfilesPaginatedQuery $query): PaginatedQueryResult
    {
        $totalCount = $this->dataManager->getRepository(TaxProfile::class)->count();
        $taxProfiles = $this->dataManager->getRepository(TaxProfile::class)
            ->findPaginated($query->pageNum, $query->pageSize);
         
        return new PaginatedQueryResult($totalCount, $query->pageNum, $query->pageSize, $taxProfiles);
    }
}