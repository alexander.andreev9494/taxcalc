<?php 


namespace App\Application\Features\TaxProfile\Validation;


use App\Application\Features\TaxProfile\Queries\GetProfilesByNameQuery;
use App\Domain\Cqrs\Contracts\QueryBusInterface;
use App\Domain\Entities\TaxProfile;
use App\Domain\Validation\Contracts\CustomValidatorInterface;
use App\Domain\Validation\Contracts\ValidationContextInterface;
use App\Domain\Validation\ValidationResult;

class TaxProfileUniqueValidator implements CustomValidatorInterface
{
    public function __construct(private QueryBusInterface $queryBus)
    {
        
    }

    /**
     * @param TaxProfileValidationContext $validationContext
     */
    public function __invoke(string $propName, string $value, ?ValidationContextInterface $validationContext = null): ValidationResult
    {
        $profiles = $this->queryBus->execute(new GetProfilesByNameQuery($value));

        if ($validationContext !== null)
        {
            $profiles = array_filter($profiles, fn (TaxProfile $p) => $p->getId() !== $validationContext->taxProfileId); 
        } 

        if (!empty($profiles))
        {
            return new ValidationResult(TaxProfile::class, $propName, "Profile with name $value already exists", ValidationResult::STATUS_ERROR);
        } 
        
        return new ValidationResult(TaxProfile::class, $propName, "", ValidationResult::STATUS_OK);
    }
}