<?php


namespace App\Application\Features\TaxProfile\Validation;


use App\Application\Abstractions\BaseValidationProfile;
use App\Domain\Cqrs\Contracts\QueryBusInterface;
use App\Domain\Entities\TaxProfile;
use App\Domain\Validation\Contracts\ValidationBuilderInterface;
use App\Domain\Validation\Contracts\ValidationEngineInterface;

class TaxProfileValidationProfile extends BaseValidationProfile
{
    public function __construct(ValidationEngineInterface $validationEngine, private QueryBusInterface $queryBus)
    {
        parent::__construct($validationEngine);
    }

    public function register(): void
    {
        $this->validationEngine->forEntity(TaxProfile::class, function (ValidationBuilderInterface $builder)
        {
            $builder->property("name")
                ->minLength(1)
                ->maxLength(64)
                ->withCustomValidator(new TaxProfileUniqueValidator($this->queryBus));

            $builder->property("description")
                ->isRequired();
        });
    }
}