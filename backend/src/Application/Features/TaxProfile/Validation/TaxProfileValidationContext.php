<?php 


namespace App\Application\Features\TaxProfile\Validation;

use App\Domain\Validation\Contracts\ValidationContextInterface;

class TaxProfileValidationContext implements ValidationContextInterface
{
    public function __construct(public int $taxProfileId)
    {
        
    }
}