<?php


namespace App\Application\Features\Income\Commands;


use App\Application\Abstractions\BaseCommandHandler;
use App\Domain\Cqrs\CommandResult;
use App\Domain\Entities\Income;

class DeleteIncomeCommandHandler extends BaseCommandHandler
{
    public function __invoke(DeleteIncomeCommand $command): CommandResult
    {
        $this->dataManager->remove(Income::class, $command->id);
        $this->dataManager->save();

        return CommandResult::ok();
    }
}