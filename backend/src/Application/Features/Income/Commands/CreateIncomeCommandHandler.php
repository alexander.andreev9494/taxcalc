<?php


namespace App\Application\Features\Income\Commands;


use App\Application\Abstractions\BaseCommandHandler;
use App\Domain\Cqrs\CommandResult;
use App\Domain\Entities\Income;
use App\Domain\Entities\TaxProfile;
use Exception;

class CreateIncomeCommandHandler extends BaseCommandHandler
{
    public function __invoke(CreateIncomeCommand $command): CommandResult
    {
        /** @var TaxProfile $profile */
        $profile = $this->dataManager->getRepository(TaxProfile::class)->findOne($command->profileId);
        if ($profile === null)
        {
            throw new Exception("Can not find profile by id: {$command->profileId}");
        }

        /** @var Income $income */
        $income = new Income($profile, $command->amount);

        $validationResults = $this->runValidation($income);

        if (!$validationResults->hasErrors())
        {
            $this->dataManager->persist($income);
            $this->dataManager->save();

            return CommandResult::ok(["id" => $income->getId(), "warnings" => $validationResults->getWarnings()]);
        }

        return CommandResult::fail($validationResults->getErrors());
    }
}