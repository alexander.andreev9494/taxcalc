<?php


namespace App\Application\Features\Income\Commands;


use App\Domain\Cqrs\Attributes\Handler;
use App\Domain\Cqrs\Contracts\CommandInterface;

#[Handler(DeleteIncomeCommandHandler::class)]
class DeleteIncomeCommand implements CommandInterface
{
    public function __construct(public int $id)
    {
        
    }
}