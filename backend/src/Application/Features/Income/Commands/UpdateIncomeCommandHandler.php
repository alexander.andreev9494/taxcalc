<?php


namespace App\Application\Features\Income\Commands;


use App\Application\Abstractions\BaseCommandHandler;
use App\Domain\Cqrs\CommandResult;
use App\Domain\Entities\Income;
use Exception;

class UpdateIncomeCommandHandler extends BaseCommandHandler
{
    public function __invoke(UpdateIncomeCommand $command): CommandResult
    {
        /** @var Income $income */
        $income = $this->dataManager->getRepository(Income::class)->findOne($command->id);
        if ($income === null)
        {
            throw new Exception("Can not update income by id: {$command->id}");
        }

        if ($income->taxProfile->getId() !== $command->profileId)
        {
            $income->taxProfile = $this->dataManager->getRepository(Income::class)->findOne($command->profileId);
            if ($income->taxProfile === null)
            {
                throw new Exception("Can not update income {$command->id}, profile not found by id {$command->profileId}");
            }
        }

        $this->mapper->map($command, $income);

        $validationResults = $this->runValidation($income);

        if (!$validationResults->hasErrors())
        {
            $this->dataManager->persist($income);
            $this->dataManager->save();

            return CommandResult::ok($validationResults->getWarnings());
        }

        return CommandResult::fail($validationResults->getErrors());
    }
}