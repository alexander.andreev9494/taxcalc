<?php


namespace App\Application\Features\Income\Commands;


use App\Domain\Cqrs\Attributes\Handler;
use App\Domain\Cqrs\Contracts\CommandInterface;

#[Handler(UpdateIncomeCommandHandler::class)]
class UpdateIncomeCommand implements CommandInterface
{
    public function __construct(public int $id,
                                public int $profileId,
                                public int $amount)
    {
        
    }
}