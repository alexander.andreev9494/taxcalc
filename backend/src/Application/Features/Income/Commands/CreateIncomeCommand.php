<?php


namespace App\Application\Features\Income\Commands;


use App\Domain\Cqrs\Attributes\Handler;
use App\Domain\Cqrs\Contracts\CommandInterface;

#[Handler(CreateIncomeCommandHandler::class)]
class CreateIncomeCommand implements CommandInterface
{
    public int $profileId;
    public int $amount;
}