<?php


namespace App\Application\Features\Income\Queries;

use App\Application\Abstractions\BaseQueryHandler;
use App\Domain\Cqrs\PaginatedQueryResult;
use App\Domain\Entities\Income;

class GetIncomePaginatedQueryHandler extends BaseQueryHandler
{
    public function __invoke(GetIncomePaginatedQuery $query): PaginatedQueryResult
    {
        $incomesAmount = $this->dataManager->getRepository(Income::class)->count();
        $incomes = $this->dataManager->getRepository(Income::class)
                                ->findPaginated($query->page, $query->pageSize);

        return new PaginatedQueryResult($incomesAmount, $query->page, $query->pageSize, $incomes);
    }
}