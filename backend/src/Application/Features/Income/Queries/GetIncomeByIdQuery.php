<?php


namespace App\Application\Features\Income\Queries;


use App\Domain\Cqrs\Attributes\Handler;
use App\Domain\Cqrs\Contracts\QueryInterface;

#[Handler(GetIncomeByIdQueryHandler::class)]
class GetIncomeByIdQuery implements QueryInterface
{
    public function __construct(public int $id) 
    {
        
    }
}