<?php


namespace App\Application\Features\Income\Queries;

use App\Application\Abstractions\BaseQueryHandler;
use App\Domain\Entities\Income;

class GetIncomeByIdQueryHandler extends BaseQueryHandler
{
    public function __invoke(GetIncomeByIdQuery $query): ?Income
    {
        return $this->dataManager->getRepository(Income::class)->findOne($query->id);
    }
}