<?php


namespace App\Application\Features\Income\Queries;


use App\Domain\Cqrs\Attributes\Handler;
use App\Domain\Cqrs\Contracts\QueryInterface;

#[Handler(GetIncomePaginatedQueryHandler::class)]
class GetIncomePaginatedQuery implements QueryInterface
{
    public function __construct(public int $page,
                                public int $pageSize) 
    {
        
    }
}