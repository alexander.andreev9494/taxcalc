<?php


namespace App\Application\Features\Tax\Queries;


use App\Domain\Cqrs\Attributes\Handler;
use App\Domain\Cqrs\Contracts\QueryInterface;

#[Handler(GetTaxByIdQueryHandler::class)]
class GetTaxByIdQuery implements QueryInterface
{
    public function __construct(public int $taxId)
    {
        
    }
}