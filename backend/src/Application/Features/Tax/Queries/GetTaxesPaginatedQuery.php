<?php


namespace App\Application\Features\Tax\Queries;


use App\Domain\Cqrs\Attributes\Handler;
use App\Domain\Cqrs\Contracts\QueryInterface;

#[Handler(GetTaxesPaginatedQueryHandler::class)]
class GetTaxesPaginatedQuery implements QueryInterface
{
    public function __construct(public int $pageNum, 
                                public int $pageSize)
    {
        
    }
}