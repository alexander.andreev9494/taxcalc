<?php


namespace App\Application\Features\Tax\Queries;


use App\Application\Abstractions\BaseQueryHandler;
use App\Domain\Entities\Tax;
use Exception;

class GetTaxByIdQueryHandler extends BaseQueryHandler
{
    public function __invoke(GetTaxByIdQuery $query): Tax
    {
        $tax = $this->dataManager->getRepository(Tax::class)->findOne($query->taxId);
        if ($tax === null)
        {
            throw new Exception("Can not find tax with id: {$query->taxId}");
        }

        return $tax;
    }
}