<?php


namespace App\Application\Features\Tax\Queries;


use App\Application\Abstractions\BaseQueryHandler;
use App\Domain\Cqrs\PaginatedQueryResult;
use App\Domain\Entities\Tax;

class GetTaxesPaginatedQueryHandler extends BaseQueryHandler 
{
    public function __invoke(GetTaxesPaginatedQuery $query): PaginatedQueryResult
    {
        $taxesCount = $this->dataManager->getRepository(Tax::class)->count();
        $taxes = $this->dataManager->getRepository(Tax::class)->findPaginated($query->pageNum, $query->pageSize);

        return new PaginatedQueryResult($taxesCount, $query->pageNum, $query->pageSize, $taxes);
    }
}