<?php


namespace App\Application\Features\Tax\Commands;


use App\Application\Abstractions\BaseCommandHandler;
use App\Domain\Cqrs\CommandResult;
use App\Domain\Entities\Tax;
use App\Domain\Entities\TaxProfile;
use Exception;

class CreateTaxCommandHandler extends BaseCommandHandler
{
    public function __invoke(CreateTaxCommand $command): CommandResult
    {
        /** @var TaxProfile $profile */
        $profile = $this->dataManager->getRepository(TaxProfile::class)->findOne($command->profileId);
        if ($profile === null)
        {
            throw new Exception("Can not find tax profile with id: {$command->profileId}");
        }

        /** @var Tax $tax */
        $tax = $this->mapper->mapByClass(Tax::class, $command);
        $tax->profile = $profile;

        $validationResults = $this->runValidation($tax);

        if (!$validationResults->hasErrors())
        {
            $this->dataManager->persist($tax);
            $this->dataManager->save();

            return CommandResult::ok(["id" => $tax->getId(), "warnings" => $validationResults->getWarnings()]);
        }

        return CommandResult::fail($validationResults->getErrors());
    }
}