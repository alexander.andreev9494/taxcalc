<?php


namespace App\Application\Features\Tax\Commands;


use App\Domain\Cqrs\Attributes\Handler;
use App\Domain\Cqrs\Contracts\CommandInterface;

#[Handler(UpdateTaxCommandHandler::class)]
class UpdateTaxCommand implements CommandInterface
{
    public int $id; 
    public ?int $profileId;
    public ?string $title;
    public ?string $description;
    public ?float $amount;
    public ?bool $isPercentage;
    public ?float $appliesBefore = null;
}