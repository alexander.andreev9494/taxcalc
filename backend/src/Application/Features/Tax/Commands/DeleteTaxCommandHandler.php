<?php


namespace App\Application\Features\Tax\Commands;


use App\Application\Abstractions\BaseCommandHandler;
use App\Domain\Cqrs\CommandResult;
use App\Domain\Entities\Tax;

class DeleteTaxCommandHandler extends BaseCommandHandler
{
    public function __invoke(DeleteTaxCommand $command): CommandResult
    {
        $this->dataManager->remove(Tax::class, $command->taxId);
        $this->dataManager->save();

        return CommandResult::ok();
    }
}