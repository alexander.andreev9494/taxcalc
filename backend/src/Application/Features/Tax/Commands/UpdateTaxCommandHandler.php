<?php


namespace App\Application\Features\Tax\Commands;


use App\Application\Abstractions\BaseCommandHandler;
use App\Domain\Cqrs\CommandResult;
use App\Domain\Entities\Tax;
use App\Domain\Entities\TaxProfile;
use Exception;

class UpdateTaxCommandHandler extends BaseCommandHandler
{
    public function __invoke(UpdateTaxCommand $command): CommandResult
    {
        /** @var Tax $tax */
        $tax = $this->dataManager->getRepository(Tax::class)->findOne($command->id);
        if ($tax === null)
        {
            throw new Exception("Can not find tax with id: {$command->id}");
        }

        if ($command->profileId !== null && $tax->profile->getId() !== $command->profileId)
        {
            $tax->profile = $this->getProfile($command->profileId);
        }

        $this->mapper->map($command, $tax);
      
        $validationResults = $this->runValidation($tax);
 
        if (!$validationResults->hasErrors())
        {
            $this->dataManager->persist($tax);
            $this->dataManager->save();

            return CommandResult::ok($validationResults->getWarnings());
        }

        return CommandResult::fail($validationResults->getErrors());
    }

    private function getProfile(int $profileId): TaxProfile
    {
        $profile = $this->dataManager->getRepository(TaxProfile::class)->findOne($profileId);
        if ($profile === null)
        {
            throw new Exception("Can not find profile with id: {$profileId}");
        }

        return $profile;
    }
}