<?php


namespace App\Application\Features\Tax\Commands;


use App\Domain\Cqrs\Attributes\Handler;
use App\Domain\Cqrs\Contracts\CommandInterface;

#[Handler(DeleteTaxCommandHandler::class)]
class DeleteTaxCommand implements CommandInterface
{
    public function __construct(public int $taxId)
    {
        
    }
}