<?php


namespace App\Application\Features\AdditionalSpend\Commands;


use App\Application\Abstractions\BaseCommandHandler;
use App\Domain\Cqrs\CommandResult;
use App\Domain\Entities\AdditionalSpend;

class DeleteAdditionalSpendCommandHandler extends BaseCommandHandler
{
    public function __invoke(DeleteAdditionalSpendCommand $command): CommandResult
    {
        $this->dataManager->remove(AdditionalSpend::class, $command->id);
        $this->dataManager->save();

        return CommandResult::ok();
    }
}