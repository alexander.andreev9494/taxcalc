<?php


namespace App\Application\Features\AdditionalSpend\Commands;


use App\Application\Abstractions\BaseCommandHandler;
use App\Domain\Cqrs\CommandResult;
use App\Domain\Entities\AdditionalSpend;
use App\Domain\Entities\TaxProfile;
use Exception;

class CreateAdditionalSpendCommandHandler extends BaseCommandHandler
{
    public function __invoke(CreateAdditionalSpendCommand $command): CommandResult
    {
        /** @var TaxProfile $profile */
        $profile = $this->dataManager->getRepository(TaxProfile::class)->findOne($command->profileId);
        if ($profile === null)
        {
            throw new Exception("Can not find profile by id: {$command->profileId}");
        }

        /** @var AdditionalSpend $spend */
        $spend = $this->mapper->mapByClass(AdditionalSpend::class, $command);
        $spend->profile = $profile;

        $validationResults = $this->runValidation($spend);

        if (!$validationResults->hasErrors())
        {
            $this->dataManager->persist($spend);
            $this->dataManager->save();

            return CommandResult::ok(["id" => $spend->getId(), "warnings" => $validationResults->getWarnings()]);
        }

        return CommandResult::fail($validationResults->getErrors());
    }
}