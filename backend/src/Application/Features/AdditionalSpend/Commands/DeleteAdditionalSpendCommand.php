<?php


namespace App\Application\Features\AdditionalSpend\Commands;


use App\Domain\Cqrs\Attributes\Handler;
use App\Domain\Cqrs\Contracts\CommandInterface;

#[Handler(DeleteAdditionalSpendCommandHandler::class)]
class DeleteAdditionalSpendCommand implements CommandInterface
{
    public function __construct(public int $id)
    {
    }
}