<?php


namespace App\Application\Features\AdditionalSpend\Commands;


use App\Domain\Cqrs\Attributes\Handler;
use App\Domain\Cqrs\Contracts\CommandInterface;

#[Handler(UpdateAdditionalSpendCommandHandler::class)]
class UpdateAdditionalSpendCommand implements CommandInterface
{
    public function __construct(public int $id,
                                public int $profileId,
                                public string $name,
                                public float $amount,
                                public bool $applyBeforeTaxation = true)
    {
    }
}