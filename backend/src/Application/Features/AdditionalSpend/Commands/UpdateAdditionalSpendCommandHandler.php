<?php


namespace App\Application\Features\AdditionalSpend\Commands;


use App\Application\Abstractions\BaseCommandHandler;
use App\Domain\Cqrs\CommandResult;
use App\Domain\Entities\AdditionalSpend;
use App\Domain\Entities\TaxProfile;
use Exception;

class UpdateAdditionalSpendCommandHandler extends BaseCommandHandler
{
    public function __invoke(UpdateAdditionalSpendCommand $command): CommandResult
    {
        /** @var AdditionalSpend $spend */
        $spend = $this->dataManager->getRepository(AdditionalSpend::class)->findOne($command->id);
        if ($spend === null)
        {
            throw new Exception("Can not find additional spend by id : {$command->id}");
        }

        /** @var TaxProfile $profile */
        $profile = $this->dataManager->getRepository(TaxProfile::class)->findOne($command->profileId);
        if ($profile === null)
        {
            throw new Exception("Can not find Tax Profile by id : {$command->profileId}");
        }

        $this->mapper->map($command, $spend);
        $spend->profile = $profile;

        $validationResults = $this->runValidation($spend);
        if (!$validationResults->hasErrors())
        {
            $this->dataManager->persist($spend);
            $this->dataManager->save();

            return CommandResult::ok($validationResults->getWarnings());
        }

        return CommandResult::fail($validationResults->getErrors());
    }
}