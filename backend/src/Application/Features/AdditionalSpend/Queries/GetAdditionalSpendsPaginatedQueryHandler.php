<?php 


namespace App\Application\Features\AdditionalSpend\Queries;


use App\Application\Abstractions\BaseQueryHandler;
use App\Domain\Cqrs\PaginatedQueryResult;
use App\Domain\Entities\AdditionalSpend;

class GetAdditionalSpendsPaginatedQueryHandler extends BaseQueryHandler
{
    public function __invoke(GetAdditionalSpendsPaginatedQuery $query): PaginatedQueryResult
    {
        $spendsAmount = $this->dataManager->getRepository(AdditionalSpend::class)->count(); 
        $spends = $this->dataManager->getRepository(AdditionalSpend::class)->findPaginated($query->page, $query->pageSize);

        return new PaginatedQueryResult($spendsAmount, $query->page, $query->pageSize, $spends);
    }
}