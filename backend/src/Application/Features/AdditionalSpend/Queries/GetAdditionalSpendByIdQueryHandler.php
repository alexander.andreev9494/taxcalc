<?php 


namespace App\Application\Features\AdditionalSpend\Queries;


use App\Application\Abstractions\BaseQueryHandler;
use App\Domain\Entities\AdditionalSpend;

class GetAdditionalSpendByIdQueryHandler extends BaseQueryHandler
{
    public function __invoke(GetAdditionalSpendByIdQuery $query): ?AdditionalSpend
    {
        return $this->dataManager->getRepository(AdditionalSpend::class)->findOne($query->id);
    }
}