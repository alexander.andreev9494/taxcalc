<?php


namespace App\Application\Features\AdditionalSpend\Queries;


use App\Domain\Cqrs\Attributes\Handler;
use App\Domain\Cqrs\Contracts\QueryInterface;

#[Handler(GetAdditionalSpendsPaginatedQueryHandler::class)]
class GetAdditionalSpendsPaginatedQuery implements QueryInterface
{
    public function __construct(public int $page,
                                public int $pageSize)
    {
        
    }
}