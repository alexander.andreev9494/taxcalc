<?php


namespace App\Application\Features\AdditionalSpend\Queries;


use App\Domain\Cqrs\Attributes\Handler;
use App\Domain\Cqrs\Contracts\QueryInterface;

#[Handler(GetAdditionalSpendByIdQueryHandler::class)]
class GetAdditionalSpendByIdQuery implements QueryInterface
{
    public function __construct(public int $id)
    {
        
    }
}