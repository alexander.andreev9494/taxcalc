<?php 


namespace App\Tests\Mocks;

use App\Domain\Cqrs\Attributes\Handler;
use App\Domain\Cqrs\Contracts\CommandInterface;

#[Handler("test_handler_class")]
class CommandMock implements CommandInterface
{

}