<?php 


namespace App\Tests\Mocks;

use App\Domain\Cqrs\Contracts\CommandInterface;
use App\Domain\Cqrs\Contracts\HandlerFactoryInterface;
use stdClass;

class HandlerFactoryMock implements HandlerFactoryInterface
{
    public function __construct(private bool $shouldReturnCallable)
    {
        
    }

    public function create(string $handlerClass): object
    {
        return $this->shouldReturnCallable ? new class
        {
            public function __invoke(CommandInterface $command): void
            {
               
            }
        } : new stdClass;
    }
}