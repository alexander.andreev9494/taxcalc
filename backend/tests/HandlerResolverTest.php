<?php


namespace App\Tests;

use App\Domain\Cqrs\Contracts\HandlerResolverInterface;
use App\Infrastructure\Cqrs\Exceptions\CanNotResolveHandlerException;
use App\Infrastructure\Cqrs\Exceptions\HandlerAttributeNotFoundException;
use App\Infrastructure\Cqrs\HandlerResolver;
use App\Tests\Mocks\CommandMock;
use App\Tests\Mocks\CommandWithoutHandlerMock;
use App\Tests\Mocks\HandlerFactoryMock;
use Closure;
use PHPUnit\Framework\TestCase;

class HandlerResolverTest extends TestCase
{
    private HandlerResolverInterface $handlerResolver;

    public function testShouldReturnHandler(): void
    {
        $this->handlerResolver = new HandlerResolver(new HandlerFactoryMock(true));

        $actual = $this->handlerResolver->resolve(new CommandMock());

        $this->assertInstanceOf(Closure::class, $actual);
    }

    public function testShouldThrowIfNotCallable(): void
    {
        $this->handlerResolver = new HandlerResolver(new HandlerFactoryMock(false));

        $this->expectException(CanNotResolveHandlerException::class);
        $this->handlerResolver->resolve(new CommandMock());
    }

    public function testShouldThrowIfCommandWithoutHandler(): void
    {
        $this->handlerResolver = new HandlerResolver(new HandlerFactoryMock(false));

        $this->expectException(HandlerAttributeNotFoundException::class);
        $this->handlerResolver->resolve(new CommandWithoutHandlerMock());
    }
}