<?php


namespace App\Tests;

use App\Domain\Dto\TaxForMonthDto;
use App\Domain\Dto\TaxForPeriodDto;
use App\Domain\Entities\Income;
use App\Domain\Entities\Tax;
use App\Domain\Entities\TaxProfile;
use App\Domain\ValueObjects\DateRange;
use DateInterval;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use PHPUnit\Framework\TestCase;

class ChangeTaskStatusUseCaseTest extends TestCase
{
    public const TAX_PERCENTAGE_AMOUNT = 5.0;

    public const AMOUNT_OF_MONEY = 3000;
    public const AMOUNT_OF_TAX = 150;

    private TaxProfile $profile;
    private TaxForMonthDto $taxForMonthDto;
    private TaxForPeriodDto $taxForPeriodDto;

    protected function setUp(): void
    {
        $this->profile = new TaxProfile("test", "test profile");
        $this->profile->incomes = new ArrayCollection([
            new Income($this->profile, 1000),
            new Income($this->profile, 1000),
            new Income($this->profile, 1000),
        ]);

        $this->profile->taxes = new ArrayCollection([
            new Tax($this->profile, "income tax", "test income tax", self::TAX_PERCENTAGE_AMOUNT, isPercentage: true)
        ]);

        $createdAt = (clone $this->profile->incomes[0]->getCreatedAt())->format("Y-m-01");
        $createdAt = new DateTime($createdAt);

        $this->taxForPeriodDto = new TaxForPeriodDto(
            new DateRange($createdAt, $createdAt),
            self::AMOUNT_OF_MONEY,
            self::AMOUNT_OF_TAX,
            self::AMOUNT_OF_MONEY - self::AMOUNT_OF_TAX
        );

        $this->taxForMonthDto = new TaxForMonthDto($this->profile->incomes[0]->getCreatedAt(), self::AMOUNT_OF_MONEY, self::AMOUNT_OF_TAX);
    }

    public function testShouldCalculateIncomeForDate(): void
    {
        $expected = 3000.0;

        $actual = $this->profile->calculateIncomeForDate($this->profile->incomes[0]->getCreatedAt());

        $this->assertEquals($expected, $actual);
    }

    public function testShouldCalculateTax(): void 
    {
        $income = 3000;
        $expected = 150.0;
        
        $actual = $this->profile->calculateTax($income);

        $this->assertEquals($expected, $actual);
    }

    public function testShouldCalculateForPeriod(): void
    {
        $createdAt = $this->profile->incomes[0]->getCreatedAt();
        $nextDay = (clone $createdAt)->add(new DateInterval("P1D"));        
      
        $expected = [$this->taxForMonthDto];

        $actual = $this->profile->calculateForPeriod(new DateRange($createdAt, $nextDay));

        $this->assertEquals($expected, $actual);
    }

    public function testShouldBuildForPeriod(): void 
    {
        $expected = $this->taxForPeriodDto;

        $actual = $this->profile->buildForPeriod(...[$this->taxForMonthDto]);

        $this->assertEquals($expected, $actual);
    }
}
