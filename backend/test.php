<?php
/*
require_once __DIR__."/vendor/autoload.php";

class TestEntity 
{
    public int $id;
    public string $name;
    public string $description;
}

interface CustomValidatorInterface
{

}

interface ValidationContextInterface
{

}

interface ValidationResultInterface
{
    public const STATUS_ERROR = 1;
    public const STATUS_WARNING = 2;
    public const STATUS_OK = 3;

    public function getMessage(): ?string;
    public function getStatus(): int;
    public function getModelName(): string;
    public function getPropertyName(): string;
}

interface ValidationRulesStageInterface
{
    public function isRequired(): ValidationRulesStageInterface;
    public function minLength(int $minLength): ValidationRulesStageInterface;
    public function maxLength(int $maxLength): ValidationRulesStageInterface;
    public function inRange(int|float $from, int|float $to): ValidationRulesStageInterface;
    public function withCustomValidator(CustomValidatorInterface $validator): ValidationRulesStageInterface;
}

interface ValidationBuilderInterface
{
    public function property(string $name): ValidationRulesStageInterface;
    public function build(): array;
}

interface ValidationEngineInterface
{
    public function forEntity(string $entityClass, Closure $builder): ValidationContextSetupStageInterface;
    public function validate(object $entity): array;
}

interface ValidationContextSetupStageInterface
{
    public function withValidationContext(ValidationContextInterface $validationContext): void;
}

class ValidationResult implements ValidationResultInterface
{
    public function __construct(private string $className,
                                private string $propName,
                                private ?string $message = null,
                                private int $status = self::STATUS_OK)
    {
        
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function getModelName(): string
    {
        return $this->className;
    }

    public function getPropertyName(): string
    {
        return $this->propName;
    }
}

class ValidationBuilder implements ValidationBuilderInterface, ValidationRulesStageInterface
{
    private array $properties = [];
    private string $propertyName = "";

    public function property(string $name): ValidationRulesStageInterface
    {
        $properties[$name] = [];
        $properties[$name]["required"] = false;
        $properties[$name]["minLength"] = false;
        $properties[$name]["maxLength"] = false;
        $properties[$name]["inRange"] = false;
        $properties[$name]["withCustomValidator"] = false;
        
        $this->propertyName = $name;

        return $this;
    }

    public function build(): array
    {
        return $this->properties;
    }
    
    public function isRequired(): ValidationRulesStageInterface
    {
        $this->properties[$this->propertyName]["required"] = true;
        return $this;
    }
    
    public function minLength(int $minLength): ValidationRulesStageInterface
    {
        $this->properties[$this->propertyName]["minLength"] = $minLength;
        return $this;
    }
    
    public function maxLength(int $maxLength): ValidationRulesStageInterface
    {
        $this->properties[$this->propertyName]["maxLength"] = $maxLength;
        return $this;
    }
    
    public function inRange(int|float $from, int|float $to): ValidationRulesStageInterface
    {
        $this->properties[$this->propertyName]["inRange"] = [$from, $to];
        return $this;
    }

    public function withCustomValidator(CustomValidatorInterface $validator): ValidationRulesStageInterface
    {
        if (!is_callable($validator))
        {
            throw new Exception("Provided object doesn't contain method __invoke(...)");
        }

        $this->properties[$this->propertyName]["withCustomValidator"] = Closure::fromCallable($validator);
        return $this;
    }
}


class ValidationEngine implements ValidationEngineInterface, ValidationContextSetupStageInterface
{
    private array $validationContainers = [];
    private string $entityClass;

    public function forEntity(string $entityClass, Closure $builder): ValidationContextSetupStageInterface
    {
        $this->entityClass = $entityClass;
        if (!isset($this->validationContainers[$entityClass]))
        {
            $validationBuilder = new ValidationBuilder();
        }
        else 
        {
            $validationBuilder = $this->validationContainers[$entityClass]["builder"];
        }
       
        $builder($validationBuilder);
        $this->validationContainers[$entityClass]["builder"] = $validationBuilder;

        return $this;
    }

    public function validate(object $entity): array
    {
        if (!isset($this->validationContainers[$entity::class]))
        {
            return [];
        }

        $validationRules = $this->validationContainers[$entity::class]["builder"]->build();
        $validationContext = $this->validationContainers[$entity::class]["_context"] ?? null;

        $props = $this->getProperties($entity);

        $results = [];
        foreach ($props as $propName => $value)
        {
            foreach ($validationRules as $validationPropName => $rules)
            {
                if ($propName !== $validationPropName)
                {
                    continue;
                }

                foreach ($rules as $rule => $ruleValue)
                {
                    if (!$rule)
                    {
                        continue;
                    }

                    $results[] = match ($rule)
                    {
                        "required" => $this->checkRequired($entity::class, $propName, $value),
                        "minLength" => $this->checkMinLength($entity::class, $propName, $value, $ruleValue),
                        "maxLength" => $this->checkMaxLength($entity::class, $propName, $value, $ruleValue),
                        "inRange" => $this->checkInRange($entity::class, $propName, $value, $ruleValue[0], $ruleValue[1]),
                        "withCustomValidator" => $ruleValue($propName, $value, $validationContext)
                    };
                }
            }
        }

        return $results;
    }

    public function withValidationContext(ValidationContextInterface $validationContext): void
    {
        $this->validationContainers[$this->entityClass]["_context"] = $validationContext;
    }

    private function getProperties(object $entity): array
    {
        $reflection = new ReflectionClass($entity::class);

        $props = $reflection->getProperties();

        $mappedProps = array_map(function (ReflectionProperty $p) use ($entity)
        {
            $changedAccess = false;
            if (!$p->isPublic())
            {
                $changedAccess = true;
                $p->setAccessible(true);
            }

            $result = [$p->getName() => $p->getValue($entity)];

            if ($changedAccess)
            {
                $p->setAccessible(false);
            }

            return $result;
        }, $props);

        return array_merge(...$mappedProps);
    }

    private function checkRequired(string $className, string $propName, $value): ValidationResultInterface
    {
        if (!$value || $value === null || empty($value) || !isset($value))
        {
            return new ValidationResult($className, $propName, "The {$propName} is required", ValidationResult::STATUS_ERROR);
        }

        return new ValidationResult($className, $propName);
    }

    private function checkMinLength(string $className, string $propName, string $value, int $minLength): ValidationResultInterface
    {
        return strlen($value) < $minLength
            ? new ValidationResult($className, $propName, "The {$propName} length less than {$minLength}", ValidationResult::STATUS_ERROR)
            : new ValidationResult($className, $propName);
    }

    private function checkMaxLength(string $className, string $propName, string $value, int $maxLength): ValidationResultInterface
    {
        return strlen($value) > $maxLength
            ? new ValidationResult($className, $propName, "The {$propName} length greater than {$maxLength}", ValidationResult::STATUS_ERROR)
            : new ValidationResult($className, $propName);
    }

    private function checkInRange(string $className, string $propName, int|float $value, int $from, int $to): ValidationResultInterface
    {
        return $value > $to && $value < $from
            ? new ValidationResult($className, $propName, "The {$propName} should be between {$from} and {$to}", ValidationResult::STATUS_ERROR)
            : new ValidationResult($className, $propName);
    }
}

class TestEntityValidationContext implements ValidationContextInterface
{
    public int $warningLength = 10;
}

class CustomValidator implements CustomValidatorInterface
{
    public function __invoke(string $propName, string $value, TestEntityValidationContext $validationContext)
    {
        if (strlen($value) < $validationContext->warningLength)
        {
            return new ValidationResult(TestEntity::class, $propName);
        }
        else
        {
            return new ValidationResult(TestEntity::class, $propName, "Abnormal length", ValidationResult::STATUS_WARNING);
        }
    }
}

$engine = new ValidationEngine();

$engine->forEntity(TestEntity::class, function (ValidationBuilderInterface $builder) 
{
    $builder->property("name")
        ->isRequired()
        ->minLength(1)
        ->maxLength(64)
        ->withCustomValidator(new CustomValidator());

    $builder->property("description")
        ->isRequired()
        ->maxLength(256);
})->withValidationContext(new TestEntityValidationContext());

$obj = new TestEntity();
$obj->name ="testsdsdfsdf";
$obj->id = 1;
$obj->description ="test_description";

$results = $engine->validate($obj);

print_r($results);*/